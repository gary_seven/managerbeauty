package com.org.managerbeauty.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.infrastructure.Fake.FakeProfissionalRepository;

public class ServicoTest {
	
	private Servico servico;
	private Profissional profissional;
	private IProfissionalRepository repository;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		servico = new Servico(new ID_Servico("1"),"corte", 20);
		repository = new FakeProfissionalRepository();
		
		profissional = new Profissional(repository.nextProfissionalId(), "joao", new Usuario("", ""));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void CanCadastreProfissional() {
		servico.cadastrarProfissional(profissional);
		assertEquals(1, servico.getNumberOfProfissionais());
	}
	
	@Test
	public void CanRemoveProfissional(){
		servico.cadastrarProfissional(profissional);
		servico.removerProfissional(profissional);
		assertEquals(0, servico.getNumberOfProfissionais());
	}
	
}
