package com.org.managerbeauty.domain.service;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;

public class ServiceTest {

	private Servico servico;
	private Servico servico2;
	private Profissional profissional;
	private Profissional profissional2;
	
	@Before
	public void setUp() throws Exception {
		servico = new Servico(new ID_Servico("1"),"servico1", 25f);
		servico2 = new Servico(new ID_Servico("2"),"servico2", 10f);
		profissional = new Profissional(new ID_Profissional("1"), "João", new Usuario("usuario", "senha"));
		profissional2 = new Profissional(new ID_Profissional("2"), "Pedro", new Usuario("usr", "12345"));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test 
	public void testSameValueAs(){
		assertFalse(servico.samaIdentityAs(servico2));
		assertTrue(servico.samaIdentityAs(new Servico(new ID_Servico("1"),"servico1", 25f)));
	}

	@Test
	public void testCadastarProfissional() {
		servico.cadastrarProfissional(profissional);
		assertEquals(1, servico.getNumberOfProfissionais());
	}
	
	@Test
	public void testRemoverProfissional(){
		servico.removerProfissional(profissional);
		assertEquals(0, servico.getNumberOfProfissionais());
	}

	@Test
	public void testAllProfissionais(){
		servico.cadastrarProfissional(profissional);
		servico.cadastrarProfissional(profissional2);
		
		ArrayList<Profissional> profissionals = new ArrayList<>();
		profissionals.add(profissional);
		profissionals.add(profissional2);
		
		assertEquals(profissionals, servico.allProfissionais());
	}
}
