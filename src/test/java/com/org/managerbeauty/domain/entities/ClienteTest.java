package com.org.managerbeauty.domain.entities;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.IClienteRepository;
import com.org.managerbeauty.infrastructure.Fake.FakeClienteRepository;

public class ClienteTest {

	private Cliente cliente;
	private IClienteRepository repository;
	
	void simulateCliente(){
		cliente = new Cliente("Jon", new CPF(""));
		cliente.setLastName("Smith");
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Before
	public void setUp(){
		repository = new FakeClienteRepository();
	}

	@Test
	public void CanCreateCliente() {
		simulateCliente();
		repository.add(cliente);
	}
	
	@Test
	public void CanSearchForName(){
		simulateCliente();
		repository.add(cliente);
		Object[] objCliente = repository.findByName("Jon Smith").toArray();
		assertTrue(objCliente[0].equals(cliente));
	}
}
