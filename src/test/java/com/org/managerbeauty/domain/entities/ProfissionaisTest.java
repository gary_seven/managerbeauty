package com.org.managerbeauty.domain.entities;

import org.junit.After;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.infrastructure.Fake.FakeProfissionalRepository;

public class ProfissionaisTest {
	
	IProfissionalRepository repository;

	@Before
	public void setUP(){
		repository = new FakeProfissionalRepository();
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void certifiesThatEachProfissionalHasUniqueId(){
	
		ID_Profissional profissionalId = repository.nextProfissionalId();
		ID_Profissional profissionalId2 = repository.nextProfissionalId();
		
		Profissional profissional = new Profissional(profissionalId, "Joao",new Usuario("", ""));
		Profissional profissional2 = new Profissional(profissionalId2, "Pedro",new Usuario("",""));
		
		assertNotEquals(profissional.getId_profissional(), profissional2.getId_profissional());
	}
	
	@Test
	public void CanFindProfissionaisOnService(){
		Servico manicure = new Servico(new ID_Servico("1"),"manicure", 2.00f);
		Profissional p = new Profissional(repository.nextProfissionalId(),"Joao", new Usuario("", ""));
		
		manicure.cadastrarProfissional(p);
		
		assertTrue(manicure.allProfissionais().contains(p));	
	}
}
