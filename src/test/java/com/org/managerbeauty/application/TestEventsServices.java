package com.org.managerbeauty.application;

import static org.junit.Assert.*;

import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;

import com.org.managerbeauty.application.EventService;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.IClienteRepository;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.domain.event.Event;
import com.org.managerbeauty.domain.event.ID_Event;
import com.org.managerbeauty.domain.event.IEventRepository;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.exceptions.ProfissionalnotValidException;

public class TestEventsServices {
	
	private EventService serviceEvent;
	private IEventRepository repositoryEvent;
	private IClienteRepository clienteRepository;
	private IProfissionalRepository profissionalRepository;
	private IServicoRepository servicoRepository;
	private Event event;
	private Servico servico;
	private Profissional profissional;
	private Profissional profissional2;
	private Cliente cliente;
	private ID_Event idEvent;
	
	public Event expectedEvent(){
		event = new Event(idEvent,cliente,servico);
		event.setStartDate(new DateTime());
		event.setEndDate(new DateTime());
		
		return event;
	}
	
	public ID_Event nextId(){
		final String random = UUID.randomUUID().toString().toLowerCase();
		
		return new ID_Event(random.substring(0, random.indexOf("-")));
	}

	public void setEntities(){
		cliente = new Cliente("Jhon", new CPF("11111111"));
		servico = new Servico(new ID_Servico("1"),"corte", 5.50f);
		Usuario usuario = new Usuario("Who","1234");
		profissional = new Profissional(new ID_Profissional("id"), "Smith", usuario);
		profissional2 = new Profissional(new ID_Profissional("id2"), "Jhon", usuario);
	}
	
	public void setMocks(){
		repositoryEvent = createNiceMock(IEventRepository.class);
		clienteRepository = createNiceMock(IClienteRepository.class);
		servicoRepository = createNiceMock(IServicoRepository.class);
		profissionalRepository = createNiceMock(IProfissionalRepository.class);
		
		expect(clienteRepository.find((CPF)anyObject())).andReturn(cliente);
		expect(servicoRepository.find((ID_Servico)anyObject())).andReturn(servico);
		expect(profissionalRepository.find(new ID_Profissional("id"))).andReturn(profissional);
		expect(profissionalRepository.find(new ID_Profissional("id2"))).andReturn(profissional2);
		
		expect(repositoryEvent.add(expectedEvent())).andReturn(expectedEvent());
		expect(repositoryEvent.nextEventId()).andReturn(idEvent);
		//expect(repositoryEvent.find((ID_Event)anyObject())).andReturn(expectedEvent());
	}
	
	public void replayAllMocks(){
		replay(repositoryEvent);
		replay(clienteRepository);
		replay(servicoRepository);
		replay(profissionalRepository);
	}
	
	@Before
	public void setUp() throws Exception {
		setEntities();
		idEvent = nextId();
		setMocks();
		
		serviceEvent = new EventService(repositoryEvent, clienteRepository, 
												profissionalRepository, servicoRepository);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBookNewEvent() {
		replayAllMocks();
		
		assertEquals(expectedEvent(), serviceEvent.bookNewEvent(new ID_Servico(""), new CPF(""), 
															new DateTime(), new DateTime()));
		verify(repositoryEvent);
		verify(clienteRepository);
		verify(servicoRepository);
	}
	
	@Test
	public void testAssignProfissional(){
		servico.cadastrarProfissional(profissional);
		
		expect(repositoryEvent.find((ID_Event)anyObject())).andReturn(expectedEvent());
		replayAllMocks();
		
		serviceEvent.bookNewEvent(new ID_Servico(""), new CPF(""), new DateTime(), new DateTime());
		
		try {
			
			serviceEvent.assignProfissional(new ID_Profissional("id2"), expectedEvent().idEvent());
			fail("Deveria lançar excessão");
		} catch (ProfissionalnotValidException e) {
			// TODO Auto-generated catch block
			assertTrue("Não disparou ProfissionalnotValidException", e instanceof ProfissionalnotValidException);
		}
	}
}
