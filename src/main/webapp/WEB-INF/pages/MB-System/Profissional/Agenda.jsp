<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round|Satisfy' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/dhtmlxscheduler.css" rel="stylesheet">
	
	
	<style type="text/css" media="screen">
	
		html, body{ height:100%; }
		#agenda{
			margin:0px;
			padding:0px;
			height:100%;
			overflow:hidden;
		}
	</style>
</head>
<body onload="init();">
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Profissional/Agenda"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Agenda</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/login"><i class="fa fa-unlock-alt fa-fw fa-lg"></i> <span>Página de login</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-edit fa-fw"></i>
         							<span>Agenda</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<!--End content header-->
         			<div class="row">
						<div class="col-sm-12">
							<div class="box blue-border">
								<div class="box-content">
									<div id="agenda">
									<div id="scheduler_here" class="dhx_cal_container" style="width:800px; height:600px;">
											<div class="dhx_cal_navline">
												<div class="dhx_cal_prev_button">&nbsp;</div>
												<div class="dhx_cal_next_button">&nbsp;</div>
												<div class="dhx_cal_today_button"></div>
												<div class="dhx_cal_date"></div>
												<div class="dhx_cal_tab" name="day_tab" style="right: 204px;">
												</div>
												<div class="dhx_cal_tab" name="week_tab" style="right: 140px;">
												</div>
												<div class="dhx_cal_tab" name="month_tab" style="right: 76px;">
												</div>
											</div>
											<div class="dhx_cal_header"></div>
											<div class="dhx_cal_data"></div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<footer>
		<p>
			<span style="text-align: left; float: left">© 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/dhtmlxscheduler.js"></script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/codebase/dhtmlxdataprocessor.js"></script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/locale/locale_pt.js" charset="utf-8"></script>

	<script>
		function init(){
			scheduler.config.xml_date="%Y-%m-%d %H:%i";
			//scheduler.config.readonly = true;
			scheduler.config.first_hour = 8;
			scheduler.config.readonly = true;
			
			var events = [
			     			{id: 1, start_date: "2014-05-10 12:00", end_date: "2014-05-10 14:00", text:"Manicure" },
			     			{id: 2, start_date: "2014-05-05 12:30", end_date: "2014-05-05 15:00", text:"Corte" },
			                {id: 3, start_date: "2014-05-07 13:30", end_date: "2014-05-07 17:00", text:"Corte" },
			                {id: 4, start_date: "2014-05-15 16:30", end_date: "2014-05-15 20:00", text:"Pedicure" },
			     		]
			
			scheduler.init('scheduler_here', new Date(),"week");
			scheduler.parse(events,"json");
		}
	</script>
</body>
</html>
