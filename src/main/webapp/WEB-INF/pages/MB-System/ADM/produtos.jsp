<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round|Satisfy' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
	
</head>
<body>
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Calendario"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Calendário</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Graficos"><i class="fa fa-bar-chart-o fa-fw fa-lg"></i> <span>Gráficos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Cliente"><i class="fa fa-users fa-fw fa-lg"></i><span>Clientes</span> </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Profissional" ><i class="fa fa-wrench fa-fw fa-lg"></i><span>Profissionais</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Produtos"><i class="fa fa-shopping-cart fa-fw fa-lg"></i> <span>Produtos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-edit fa-fw"></i>
         							<span>Gestão de Produtos</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<!--End content header-->
         			<div class="row">
						<div class="col-sm-12">
							<div class="box blue-border">
								<div class="box-content">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>Código</th>
													<th>Produto</th>
													<th>
														<div class="text-center">Quantidade</div>
													</th>
													<th>
														<div class="text-right">Preço</div>
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>AO2</td>
													<td>Bien Professional Resistance</td>
													<td>
													  <div class="text-center">2</div>
													</td>
													<td>
													  <div class="text-right">$38.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>AO4</td>
													<td>Bien Professional Control Repair Shampoo</td>
													<td>
													  <div class="text-center">3</div>
													</td>
													<td>
													  <div class="text-right">$30.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>AO8</td>
													<td>Lanza Healing Nourish Tratamento </td>
													<td>
													  <div class="text-center">1</div>
													</td>
													<td>
													  <div class="text-right">$42.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>AO3</td>
													<td>Hair Sink Fresh</td>
													<td>
													  <div class="text-center">1</div>
													</td>
													<td>
													  <div class="text-right">$33.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>AO4</td>
													<td>Schwarzkopf - Coloração</td>
													<td>
													  <div class="text-center">3</div>
													</td>
													<td>
													  <div class="text-right">$350.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>AO7</td>
													<td>Vizcaya Kit</td>
													<td>
													  <div class="text-center">1</div>
													</td>
													<td>
													  <div class="text-right">$211.00</div>
													</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" href="#">
																<i class="fa fa-check" data-toggle="modal" data-target="#editModal" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs" href="#">
																<i class="fa fa-times" data-toggle="modal" data-target=".bs-example-modal-sm" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="form-group">
										<div class="text-right">
											<a class="btn btn-primary" " data-toggle="modal" data-target="#editModal" ><i class="fa fa-plus"></i> Novo</a>
										
											<a class="btn" data-toggle="modal" data-target="#movModal" ><i class="fa fa-repeat"></i> Movimentação</a>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<!-- Modal -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Produto</h4>
		  </div>
		  <div class="modal-body">
				<form class="form-horizontal" role="form">
				  <div class="form-group">
					<label for="nomeProduto" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="nomeProduto" value="Bien Professional Resistance">
					</div>
				  </div>
				  <div class="form-group">
					<label for="descricaoProduto" class="col-sm-2 control-label">Descricao</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="descricaoProduto" value="Nutrição capilar">
					</div>
				  </div>
				  <div class="form-group">
					<label for="precoProduto" class="col-sm-2 control-label" >Preço</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="precoProduto" value="R$38.00">
					</div>
				  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			<button type="button" class="btn btn-primary">Salvar</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- Modal2 -->
	<div class="modal fade" id="movModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Produtos - Movimentação</h4>
		  </div>
		  <div class="modal-body">
				<form class="form-horizontal" role="form">
				  <div class="form-group">
					<label for="nomeProduto" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="nomeProduto" text="">
					</div>
				  </div>
				  <div class="form-group">
					<label for="descricaoProduto" class="col-sm-2 control-label">Descricao</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="descricaoProduto" text="">
					</div>
				  </div>
				  <div class="form-group">
					<label for="precoProduto" class="col-sm-2 control-label">Preço</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="precoProduto" text="">
					</div>
				  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			<button type="button" class="btn btn-primary">Salvar</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- Alert Box --> 	
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p>Esta ação excluirá o produto. Deseja continuar?</p>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="button" class="btn btn-primary">Continuar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<footer>
		<p>
			<span style="text-align: left; float: left">© 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
</body>
</html>
