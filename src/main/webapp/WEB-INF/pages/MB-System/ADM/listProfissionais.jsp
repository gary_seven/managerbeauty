<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
</head>
<body>
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Calendario"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Calend�rio</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Graficos"><i class="fa fa-bar-chart-o fa-fw fa-lg"></i> <span>Gr�ficos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Cliente"><i class="fa fa-users fa-fw fa-lg"></i><span>Clientes</span> </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Profissional" ><i class="fa fa-wrench fa-fw fa-lg"></i><span>Profissionais</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Produtos"><i class="fa fa-shopping-cart fa-fw fa-lg"></i> <span>Produtos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-edit fa-fw"></i>
         							<span>Profissionais</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<!--End content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div class="box blue-border">
								<div class="box-content">
									<!-- Begin table  -->
									 <% System.out.println("jsp: profissional " + pageContext.findAttribute("profissionais")); %> 
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>ID</th>
													<th>Nome</th>
													<th>E-mail.</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="profissional" items="${profissionais}">
												<tr>
													<td>${profissional.id_profissional.idString}</td>	
													<td>${profissional.firstName}</td>
													<td>${profissional.email}</td>
													<td>
													  <div class="text-right">
															<a class="btn btn-success btn-xs" 
															href="#">
																<i class="fa fa-check" style="color:#FFFFFF;"></i>
															</a>
															<a class="btn btn-danger btn-xs"  
															data-toggle="modal" data-target=".bs-example-modal-sm" href="#">
																<i class="fa fa-times" style="color:#FFFFFF;"></i>
															</a>
														</div>
													</td>
												</tr>	
												</c:forEach>
											</tbody>
										</table>
									</div>
									<!-- End table -->	
									<div class="form-group">
										<div class="text-right">
											<a class="btn btn-primary" 
											href="${pageContext.request.contextPath}/System/Admin/profissional-form"><i class="fa fa-plus"></i> Novo</a>
										</div>	
									</div>
								</div>
         					</div>
         				</div>
         			</div>
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<!-- Alert Box --> 	
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p>Esta a��o excluir� o produto. Deseja continuar?</p>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a id="go" class="btn btn-primary" style="color:#ffff">Continuar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p>
			<span style="text-align: left; float: left">� 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
	
	<SCRIPT type="text/javascript">
		/*$(document).ready(function(){
			$(".btn-danger").click(function(){
				var cpf = $(this).data("cpf");
				var url = "${pageContext.request.contextPath}/System/Admin/Cliente/delete/"+cpf;
				$("#go").attr("href",url);
			});
		});*/
	</SCRIPT>
		
</body>
</html>
