<%-- 
    Document   : NewUser
    Created on : 28/11/2013, 08:08:06
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New User</title>
        
    </head>
    <body>
        <jsp:include page="Header.jsp"/>

        <div class="container-fluid">
            <div class="row-fluid">
                <!-- Main Menu -->
                <div id="side-menu" class="span2" style="color:#3a3a3a">
                    <div class="nav-collapse">
                        <ul class="nav nav-tabs nav-stacked">
                            <li><a href="index.jsp"><i class="icon-home"></i><span class="hidden-tablet"> Inicio</span></a></li>
                            <li><a href="#"><i class="icon-phone-sign"></i><span class="hidden-tablet"> Serviços</span></a></li>
                            <li><a href="#"><i class="icon-envelope"></i><span class="hidden-tablet"> menu</span></a></li>
                            <li><a href="calendar.jsp"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendário</span></a></li>
                            <li><a href="Users.jsp"><i class="icon-user"></i><span class="hidden-tablet"> Usuários</span></a></li>
                            <li><a href="../../Login.jsp"><i class="icon-lock"></i><span class="hidden-tablet"> Tela de login</span></a></li>

                        </ul>
                    </div>
                </div>
                <!-- End Main Menu -->

                <!-- Start Content -->
                <div id="content" class="span10" style="min-height: 280px;">
                    <div>
                        <div class="row-fluid s box" style="border-radius: 2px;">
                            <div class="box-header"><h2><i class=""></i> Novo Administrador</h2></div>
                            <form id="new_adm_form" class="form-horizontal" method="GET" action="NewADM.do">
                                <input name="permissao" type="hidden" value="a">
                                <div class="control-group">
                                    <label for="nome" class="control-label">	
                                        Nome:  
                                    </label>
                                    <div class="controls">
                                        <input name="nome" type="text" value="" id="nome">    
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="username" class="control-label">	
                                        Nome de Usuario:
                                    </label>
                                    <div class="controls">
                                        <input name="username" type="text" value="">
                                    </div>                 
                                </div>
                                <div class="control-group">
                                    <label for="senha" class="control-label">	
                                        Senha:
                                    </label>
                                    <div class="controls">
                                        <input name="senha" type="password" value="">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="address" class="control-label">	
                                        Logradouro:
                                    </label>
                                    <div class="controls">
                                        <input name="address" placeholder="W 123 Street" type="text" value="" id="address">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="bairro" class="control-label">	
                                        Bairro:
                                    </label>
                                    <div class="controls">
                                        <input name="bairro" type="text" value="" id="address">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="city" class="control-label">index	
                                        Cidade:
                                    </label>
                                    <div class="controls">
                                        <input name="city" type="text" value="" id="city">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-large btn-primary">Novo Usuario</button>
                                </div>
                            </form>
                             <div id="message-return" class="alert alert-success"></div>
                        </div>
                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- /fluid-row -->
        </div>
        <div class="clearfix"></div>
        <footer>
            <p>
                <span style="text-align:left;float:left">© <a href="" target="_blank">creativeLabs</a> 2013</span>
            </p>
        </footer>
         <script src="../../scripts/form-plugin.js"></script>
        <script>
            
                $(document).ready(function() {
                    $("#message-return").hide();
                    var options = {
                        target:        '#message-return',   
                        success:       showResponse  
                    };

                    // bind form using 'ajaxForm'
                    $('#new_adm_form').ajaxForm(options);
                });
            
            
       
            // post-submit callback 
            function showResponse(responseText, statusText, xhr, $form)  {
                $("#message-return").show(3);
                $("#message-return").fadeIn('slow');
            } 

        </script>
    </body>
    
</html>
