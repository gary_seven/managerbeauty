<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round|Satisfy' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/forms.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/calendar.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/dhtmlxscheduler.css" rel="stylesheet">
	
	<style type="text/css">
	#my_form {
			position: absolute;
			top: 100px;
			left: 200px;
			z-index: 10001;
			display: none;
			background-color: white;
			border: 2px outset gray;
			padding: 20px;
			font-family: Tahoma;
			font-size: 10pt;
		}
	</style>
</head>
<body>
	<div id="my_form">
	<table>
	<tr>
		<td><label for="description">Event text </label></td>
		<td><input type="text" name="description" value="" id="description"></td>
	</tr>
	<tr/>
	<tr>
		<td><label for="custom1">Profissional </label></td>
		<td><select name="custom-prof" value="" id="custom-prof" style="width: 133px;"></select></td>
	</tr>
	<tr/>
	<tr>
		<td><label for="custom2">Cliente </label></td>
		<td><select name="custom-client" value="" id="custom-client" style="width: 133px;"></select></td>
	</tr>
	<tr/>
	<tr>
		<td><label for="custom3">Servico </label></td>
		<td><select name="custom-servico" value="" id="custom-servico" style="width: 133px;"></select></td>
	</tr>
	</table>
	<br><br>
		<input type="button" name="save" value="Save" id="save" style='width:100px;' onclick="save_form()">
		<input type="button" name="close" value="Close" id="close" style='width:100px;' onclick="close_form()">
		<input type="button" name="delete" value="Delete" id="delete" style='width:100px;' onclick="delete_event()">
	</div>
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Calendario"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Calendário</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Graficos"><i class="fa fa-bar-chart-o fa-fw fa-lg"></i> <span>Gráficos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Cliente"><i class="fa fa-users fa-fw fa-lg"></i><span>Clientes</span> </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Profissional" ><i class="fa fa-wrench fa-fw fa-lg"></i><span>Profissionais</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Produtos"><i class="fa fa-shopping-cart fa-fw fa-lg"></i> <span>Produtos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-edit fa-fw"></i>
         							<span>Calendário</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<div class="row">
						<div class="col-sm-12">
							<div class="box blue-border">
								<div class="box-content">
									<form class="form-inline">
										<div class="form-group col-md-3">
											<label class="sr-only" for="inputServico">Serviço</label>
											<select class="form-control" id="inputServico">
												<option>-selecione-</option>
											</select>
										</div>
										<div class="form-group col-md-3">
											<label class="sr-only" for="inputProfissional">Profissional</label>
											<select class="form-control" id="inputProfissional">
											</select>
										</div>
										<div class="form-group col-sm-3">
											<div id="datetimepicker" class="input-group">
												<input data-format="dd/MM/yyyy" class="form-control" id="inputData" type="text"></input>
												<span class="add-on input-group-addon">
													<i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar-o"></i>
												</span>
											</div>
										</div>
										<a onclick="init();" class="btn btn-default" id="verCalendario">Ver Calendário</a>
									</form>
								</div>
							</div>
							<div class="box blue-border">
								<div class="box-content">
										<div id="scheduler_here" class="dhx_cal_container" style="width:700px; height:500px; position:relative;">
											<div class="dhx_cal_navline">
												<div class="dhx_cal_prev_button">&nbsp;</div>
												<div class="dhx_cal_next_button">&nbsp;</div>
												<div class="dhx_cal_today_button"></div>
												<div class="dhx_cal_date"></div>
												<div class="dhx_cal_tab" name="day_tab" style="right: 204px;">
												</div>
												<div class="dhx_cal_tab" name="week_tab" style="right: 140px;">
												</div>
												<div class="dhx_cal_tab" name="month_tab" style="right: 76px;">
												</div>
											</div>
											<div class="dhx_cal_header"></div>
											<div class="dhx_cal_data"></div>
										</div>
									</div>
							</div>
						</div>
         			</div>
         			<!--End content header-->
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<footer>
		<p>
			<span style="text-align: left; float: left">© 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js">
	</script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.pt-BR.js">
	</script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/dhtmlxscheduler.js"></script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/codebase/dhtmlxdataprocessor.js"></script>
	<script src="${pageContext.request.contextPath}/resources/dhtmlxScheduler_v40_std_130813/sources/locale/locale_pt.js" charset="utf-8"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/Calendar.js" charset="utf-8"></script>
	
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker').datetimepicker({
				language: 'pt-BR'
			});
		});
		
		$(document).ready(function(){
			var servicos = new Array();
			var clientes = new Array();
			var allProfissionais = new Array();
			
			function callBack(data){
				servicos = data.slice();
			}
			
			$.ajax({
				type : "GET",
				url : "/ManagerBeauty/System/Admin/Servicos",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				success : function(data) {
					console.log(data);
					callBack(data);
					$.each(servicos, function(index, item){
						$("#inputServico").append(
							$("<option></option>") // Yes you can do this.
				            .text(item.description)
				            .val(item.id_Servico.id)
						);
					});
				}
			});
			
			$.ajax({
				type : "GET",
				url : "/ManagerBeauty/System/Admin/Cliente-json",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				success : function(data) {
					console.log(data);
					clientes = data.slice();
					
					$.each(clientes, function(index, item){
						$("#custom-client").append(
							$("<option></option>") // Yes you can do this.
				            .text(item.firstName + " " + item.lastName)
				            .val(item.cpf.cpf)
						);
					});
				}
			});
			
			$.ajax({
				type : "GET",
				url : "/ManagerBeauty/System/Admin/Profissional-json",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				success : function(data) {
					console.log(data);
					allProfissionais = data.slice();
					
					$.each(allProfissionais, function(index, item){
						$("#custom-prof").append(
							$("<option></option>") // Yes you can do this.
				            .text(item.firstName + " " + (item.lastName))
				            .val(item.id_profissional.id)
						);
					});
				}
			});
			
			$.ajax({
				type : "GET",
				url : "/ManagerBeauty/System/Admin/Servicos",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				success : function(data) {
					console.log(data);
					callBack(data);
					$.each(servicos, function(index, item){
						$("#custom-servico").append(
							$("<option></option>") // Yes you can do this.
				            .text(item.description)
				            .val(item.id_Servico.id)
						);
					});
				}
			});
			
			$("#inputServico").change(function(){
				var id = $(this).val();
				var profissionais = new Array();
				$.each(servicos, function(index, item){
					if (item.id_Servico.id == id)
						profissionais = item.profissionais.slice();
				});
				$.each(profissionais, function(index, item){
					$("#inputProfissional").append(
							$("<option></option>") // Yes you can do this.
				            .text(item.firstName + " " + item.lastName)
				            .val(item.id_profissional.id)
						);
				});
			});
		});
	</script>
</body>
</html>
