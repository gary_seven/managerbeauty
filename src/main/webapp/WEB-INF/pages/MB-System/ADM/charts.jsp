<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round|Satisfy' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/charts.css" rel="stylesheet">
	
</head>
<body>
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Calendario"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Calendário</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Graficos"><i class="fa fa-bar-chart-o fa-fw fa-lg"></i> <span>Gráficos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Cliente"><i class="fa fa-users fa-fw fa-lg"></i><span>Clientes</span> </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Profissional" ><i class="fa fa-wrench fa-fw fa-lg"></i><span>Profissionais</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Produtos"><i class="fa fa-shopping-cart fa-fw fa-lg"></i> <span>Produtos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-bar-chart-o fa-fw"></i>
         							<span>Gráficos</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<!--End content header-->
         			<div class="row">
						<div class="col-sm-12">
							<div class="box">
								<div class="box-header">
									<div class="title">
										<span><h3>Indices de serviços</h3></span>
									</div>
								</div>
								<div class="box-content">
									<div id="chart-box">
										<table id="servicos-chart"
										SUMMARY="Indices de serviços no ultimo mês">
										<CAPTION ALIGN="bottom">
											Indices de serviços no ultimo mês
										</CAPTION>
											<tbody>
												<tr>
													<th>Manicure</th>
													<th>Corte</th>
													<th>Pedicure</th>
													<th>Progressiva</th>
												</tr>
												<tr>
													<td><span>34</span></td>
													<td><span>22</span></td>
													<td><span>18</span></td>
													<td><span>40</span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="box-content">
									<div id="chart-box2">
										<table id="servicos-chart-2"
										SUMMARY="Indices de serviços no ultimo trimestre">
										<CAPTION ALIGN="bottom">
											Indices de serviços no ultimo trimestre
										</CAPTION>
											<tbody>
												<tr>
													<th>Manicure</th>
													<th>Corte</th>
													<th>Pedicure</th>
													<th>Progressiva</th>
												</tr>
												<tr>
													<td><span>50</span></td>
													<td><span>65</span></td>
													<td><span>95</span></td>
													<td><span>80</span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<footer>
		<p>
			<span style="text-align: left; float: left">© 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/graphUp.js"></script>
	
	<script>
		
	$(document).ready(function() {
		
		// Second table
		$("#servicos-chart td").graphup({
			min: 0,
			decimalPoint: ',',
			cleaner: 'strip',
			painter: 'bars',
			barsAlign: 'bottom',
			colorMap: 'greenPower'
		});
		
		$("#servicos-chart-2 td").graphup({
			min: 0,
			decimalPoint: ',',
			cleaner: 'strip',
			painter: 'bars',
			barsAlign: 'bottom',
			colorMap: 'greenPower'
		});
	});
	</script>
</body>
</html>
