<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Manager's Beauty</title>
	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round|Satisfy' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
	
	<link href="${pageContext.request.contextPath}/resources/css/reset.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/basic-structure.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/ui-components.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/resources/css/forms.css 	verificaEmail: true" rel="stylesheet">
	
	<style type="text/css">
	.error{
	    color: #ffffff;
	    background-color: rgba(236, 94, 90,0.6);
	    border-color: rgba(238, 77, 99,1);
	    padding: 4px;
	    width: 100%;
	    margin-bottom: 20px;
	    border: 1px solid transparent;
	    border-radius: 2px;
	}
	</style>
	
</head>
<body>
	<div id="page">
		<jsp:include page="Header.jsp" />
		<nav id="leftmenu" class="navbar-default" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Calendario"><i class="fa fa-calendar fa-fw fa-lg"></i> <span>Calendário</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Graficos"><i class="fa fa-bar-chart-o fa-fw fa-lg"></i> <span>Gráficos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Cliente"><i class="fa fa-users fa-fw fa-lg"></i><span>Clientes</span> </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/System/Admin/Profissional" ><i class="fa fa-wrench fa-fw fa-lg"></i><span>Profissionais</span></a>
                    </li>
                    <li class="">
                        <a href="${pageContext.request.contextPath}/System/Admin/Produtos"><i class="fa fa-shopping-cart fa-fw fa-lg"></i> <span>Produtos</span></a>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- End sidebar -->
        
        <!--  Main div -->
        <div id="main-content">
        <div class="container">
         	<div class="row">
         		<div class="col-xs-12">
         			<!--content header-->
         			<div class="row">
         				<div class="col-sm-12">
         					<div id="content-header">
         						<h1 class="pull-left">
         							<i class="fa fa-edit fa-fw"></i>
         							<span>Profissional</span>
         						</h1>
         					</div>
         				</div>
         			</div>
         			<div class="row">
         				<div class="col-sm-12">
         					<div class="box blue-border">
								<div class="box-header blue-background">
									<div class="title">
										<i class="fa fa-pencil"></i>
										Formulário Profissional
									</div>
								</div>
								<div class="box-content box-double-padding">
									<form class="form" id="profissional-form">
										<fieldset>
											<div class="col-sm-4">
												<div class="box">
													<div class="lead">
														<i class="fa fa-info-circle"></i>
														Dados Pessoais
													</div>
													<small class="text-muted">
														Preencha comos dados pessoais do Profissional
													</small>
												</div>
												<div class="box" id="message">
													
												</div>
											</div>
											<div class="col-sm-7 col-sm-offset-1">
												<div class="form-group">
													<label>Primeiro Nome</label>
													<input class="form-control" type="text" name="firstName" id="firstName">
													
												</div>
												<div class="form-group">
													<label>Sobrenome</label>
													<input class="form-control" type="text" name="lastName" id="lastName">
													
												</div>
												<div class="form-group">
													<label>Telefone</label>
													<input class="form-control" type="text" name="phone" id="phone">
													
												</div>
												<div class="form-group">
													<label>E-mail</label>
													<input class="form-control" type="email" name="text" id="email">
													
												</div>
												<div class="form-group">
													<label>Logradouro</label>
													<input class="form-control" placeholder="Rua Ouro Preto n 34" type="text"
													name="logradouro" id="logradouro">
												</div>
												<div class="form-group">
													<label>Complemento</label>
													<input class="form-control" placeholder="Casa D" type="text" name="complemento" 
													id="complemento" value="">
												</div>
												<div class="form-group">
													<label>Cidade</label>
													<input class="form-control" placeholder="Cidade" type="text" name="cidade" id="cidade">
												</div>
											</div>
										</fieldset>
										<hr class="hr-default">
										<fieldset>
											<div class="col-sm-4">
												<div class="box">
													<div class="lead">
														<i class="fa fa-info-circle"></i>
														Usuário do Sistema
													</div>
													<small class="text-muted">
														Preecha com as informações que o profissional utilizará para acessar o sistema.
													</small>
												</div>
											</div>
											<div class="col-sm-7 col-sm-offset-1">
												<div class="form-group">
													<label>Username</label>
													<input class="form-control" placeholder="username@salao.com" type="text" name="username" id="username">
													
												</div>
												<div class="form-group">
													<label>Senha</label>
													<input class="form-control" type="password" name="senha" id="senha">
												</div>
												
											</div>
										</fieldset>
										<div class="form-actions">
											<div class="row">
												<div class="col-sm-7 col-sm-offset-5">
													<button class="btn btn-primary btn-lg" type="submit">
														<i class="fa fa-save"></i>
														Save
													</button>
													<button class="btn btn-lg" type="reset">Reset</button>
												</div>
											</div>
										</div>
									</form>
								</div>
         					</div>
         				</div>
         			</div>
         			<!--End content header-->
         		</div>
         	</div>
        </div>
        </div>
        <!-- Main div -->	
	</div>
	<footer>
		<p>
			<span style="text-align: left; float: left">© 
				<a href=""target="_blank">The Big Company Corporation</a> 2013
			</span>
		</p>
	</footer>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/jquery.validate.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/masked.js"></script>
	<script src="${pageContext.request.contextPath}/resources/scripts/ProfissionalValidation.js" type="text/javascript"></script>
</body>
</html>
