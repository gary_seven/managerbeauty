<%-- 
    Document   : Header
    Created on : 13/10/2013, 09:29:08
    Author     : andre
--%>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
    <head>
                
        <script type="text/javascript"> 
            $(document).ready(function () { 
                $('.dropdown-toggle').dropdown(); 
            }); 
        </script>
        
    </head>
    <body>
       	<nav class="navbar navbar-default" role="navigation">
       		<div class="container-fluid">
       			<a class="navbar-brand" href="#">
       				<span class="manager">Manager.<span class="beauty">beauty( )</span>;</span>
       			</a>
       			<div class="navbar-collapse collapse">
       				<ul id="drop-menu" class="nav navbar-top-links navbar-right">
       					<li class="dropdown">
       						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
       							${pageContext.request.userPrincipal.name}<b class="caret"></b>
       						</a>
       						<ul id="menu-droped" class="dropdown-menu" role="menu">
				                <li><a href="#">&nbsp;</a></li>
				                <li class="divider"></li>
				                <li><a href="<c:url value="/j_spring_security_logout"> </c:url>">Sair</a></li>
			              	</ul>
       					</li>
       				</ul>
       			</div>
       		</div>
       </nav>

    </body>
</html>
