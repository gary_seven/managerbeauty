<%-- 
    Document   : Users
    Created on : 26/11/2013, 10:34:03
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Users's</title>
       
        <link href="../../css/search-bar.css" rel="stylesheet">
        
        <script>
            $(document).ready(function(){
                var btns = ['btn-one', 'btn-two'];
                var input = document.getElementById('btn-input');
                input.value = "Administrador";
                for(var i = 0; i < btns.length; i++) {
                    document.getElementById(btns[i]).addEventListener('click', function() {
                    input.value = this.value;
                });
                
                $("table").hide();
                $("a#delete").click(function(){
                   var row = $(this).closest('tr');
                   row.hide();
    
                });
            }
            });
            
        </script>
        
    </head>
    <body>
        <jsp:include page="Header.jsp"/>
        
        <div class="container-fluid">
            <div class="row-fluid">
                <!-- Main Menu -->
                <div id="side-menu" class="span2" style="color:#3a3a3a">
                    <div class="nav-collapse">
                        <ul class="nav nav-tabs nav-stacked">
                            <li><a href="index.jsp"><i class="icon-home"></i><span class="hidden-tablet"> Inicio</span></a></li>
                            <li><a href="#"><i class="icon-phone-sign"></i><span class="hidden-tablet"> Serviços</span></a></li>
                            <li><a href="#"><i class="icon-envelope"></i><span class="hidden-tablet"> menu</span></a></li>
                            <li><a href="calendar.jsp"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendário</span></a></li>
                            <li><a href="#"><i class="icon-user"></i><span class="hidden-tablet"> Usuários</span></a></li>
                            <li><a href="/Manager-Beauty/Login.jsp"><i class="icon-lock"></i><span class="hidden-tablet"> Tela de login</span></a></li>
                            
                        </ul>
                    </div>
                </div>
                <!-- End Main Menu -->
                
                <!-- Start Content -->
                <div id="content" class="span10" style="min-height: 280px;">
                    <div>
                        <div class="row-fluid s box" style="border-radius: 2px;">
                             <div class="box-header"><h2><i class=""></i> Buscar usuário</h2></div>
                             <form id='custom-search-form' name='custom-search' class="form-search" action="Search.do" method="POST">
                                <!-- <div class="container"> 
                                     <div class="row">-->
                                         <input type="hidden" name="option" value="" id="btn-input" />
                                         <div class="span7">
                                             <div class="pull-right btn-group" data-toggle="buttons-radio">
                                                 <button type="button" id='btn-one' class="btn btn-primary active" value="Administrador">Administrador</button>
                                                 <button type="button" id='btn-two' class="btn btn-primary" value="Profissional">Profissional</button>
                                             </div>
                                             <div class="span5">
                                                 <div class="input-append span5">
                                                     <input type="text" name="search" class="search-query" placeholder="Search">
                                                     <button id='button-search' type="submit" class="btn"><i class="icon-search"></i></button>
                                                 </div>
                                             </div>
                                         </div>
                                    <!--  </div>
                                </div>-->
                             </form>
                        </div>
                        <br>
                        <div class="row-fluid s box" style="border-radius: 2px;">
                            <div class="box-header" style="background: none; margin-bottom: 0;"></div>
                            <div class="container-fluid box">
                                <table class="table TableHeadingColor">
                                    <tr>
                                        <th>Nome</th>
                                        <th>Nome de usuario</th>
                                    </tr>
                                    <tbody id="result" class="table table-bordered">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        
                        <div id="delete-response"></div>
                        <div class="form-actions" style="background: none; border: none;">
                            <button type="button" onclick="newUser();" class="btn btn-large btn-primary">Novo</button>
                        </div>
                    </div>
                </div>
                <!-- End Content -->
                
            </div>
            <!-- /fluid-row -->
        </div>
        <div class="clearfix"></div>
        <footer>
            <p>
                <span style="text-align:left;float:left">© <a href="" target="_blank">creativeLabs</a> 2013</span>
            </p>
        </footer>
        <script src="../../scripts/form-plugin.js"></script>
        <script>
            
                $(document).ready(function() {
                    var options = {
                        target:        '#result',   
                        success:       showResponse  
                    };

                    // bind form using 'ajaxForm'
                    $('#custom-search-form').ajaxForm(options);
                });
                
                function deleteRow(userName){
                    confirm("Esta ação apagará este dado permanentemente!");
                    $.get("deleteUser.do",{userName:userName},function(data){
                       $("div#delete-response").html(data);
                    }); 
                }
     
            function showResponse(responseText, statusText, xhr, $form)  {
                $("#result").fadeIn('slow');
                $("table").show();
            }
            
            function newUser(){
                var option = $("input#btn-input").val();
              
                if (option === "Administrador")
                    window.location.replace("http://localhost:8080/Manager-Beauty/MB-System/ADM/NewAdm.jsp");
                else if (option === "Profissional")
                    window.location.replace("http://localhost:8080/Manager-Beauty/MB-System/ADM/NewProfissional.jsp");
                return false;    
            }

        </script>
    </body>
</html>
