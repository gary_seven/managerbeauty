<!DOCTYPE html>
<html>
<head>
	<meta content="text/html; charset=UTF-8"> 
	<title>Manager's Beauty</title>
	
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.css" type="text/css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap-responsive.css" type="text/css" rel="stylesheet" />
	<link href="${pageContext.request.contextPath}/resources/css/styleGlobal.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<!-- Cabe�alho e menu superioir -->
	<header class="page-header" style="margin-bottom:0px;">
		<div class="container" >
			<div class="col-left">
				<div>
					<a href="#"><img src="${pageContext.request.contextPath}/resources/logo/MB-logo-head2.png" /></a>
				</div>
			</div>
			<div class="col-right">
				<nav> <!--class="collapse navbar-collapse bs-navbar-collapse"-->
				<ul class="menu-l">
					<li><a href="#">RECURSOS</a></li>
					<li><a href="#">CONTATO</a></li>
					<li><a href="${pageContext.request.contextPath}/login">LOGIN</a></li>
				</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- Fim do Cabe�alho -->
	
	<!-- Div principal com Marketing -->
	<div class="hero-unit" style="-webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
		<div class="container">
			<h2 style="margin-bottom:200px; margin-top:90px;">Um <span style="color:black;">sistema</span> para ajudar a gerenciar <span style="color:black;">sal�es de beleza.</span>
			Construido <span style="color:black;">na web</span>, te d� praticidade e mobilidade. </h2>	
		</div>
		<div class="container marketing">
			<div id="grid">
				<div class="row">
					<div class="col-lg-4">
						<img src="${pageContext.request.contextPath}/resources/images/windows_redesign/WindowsRedesign/Png/rede.png" style="height:140px; weight:140px;"/>
						<h2>Online</h2>
						<h4>Organize sua vida profissional, mesmo estando no conforto de sua casa.</h4>
					</div>
					<div class="col-lg-4">
						<img src="${pageContext.request.contextPath}/resources/images/windows_redesign/WindowsRedesign/Png/computer.png" style="height:140px; weight:140px;" />
						<h2>Pr�tico</h2>
						<h4>Dispensa qualquer infraestrutura. Voc� n�o vai precisar instalar nada em seu PC.</h4>
					</div>
					<div class="col-lg-4">
						<img src="${pageContext.request.contextPath}/resources/images/windows_redesign/WindowsRedesign/Png/idea.png" style="height:140px; weight:140px;">
						<h2>Fant�stico</h2>
						<h4>Livre-se de sistemas super complicados e da responsabilidade de mante-los. Foque-se no seu neg�cio.</h4>
					</div>
				
				</div>
			</div>
			<div class="featurette-divider" style="margin-bottom:60px;"></div>
		</div>
    </div>
	<!-- -->
	<footer>
		<hr style="border-color:#292929;">
		<div class="container">
			<p>COPYRIGHT � 2013 - 2014 Company, Inc.</p>
		</div>
	</footer>
	
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-2.0.2.intellisense.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap3/js/bootstrap.js"></script>
</body>
</html>