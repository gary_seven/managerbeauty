<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ManagerBeauty - Login</title>
        <link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/bootstrap3/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
		
		<style>
			#login-form {	margin: 0;	padding: 0;	position: absolute; top: 50%;
							margin-top: -190px;	left: 50%; margin-left: -120px;	}
		</style>
    </head>
    <body>
        <div id="fullscreen_bg" class="fullscreen_bg">

        <div class="container">
			<p>
				<c:if test="${error == true}">
					<div class="error alert alert-danger">
					<STRONG>Tente de Novo.</STRONG>
					 Nome de usuario ou senha inv�lidos.
					</div>
				</c:if>
			</p>

			<form name="login-form" id="login-form" class="form-signin" method="POST" action="<c:url value='j_spring_security_check'/>">
                <h1 class="form-signin-heading text-muted">Entrar</h1>
                <input name="j_username" type="text" class="form-control" id="j_username" placeholder="Usu�rio"><!-- pattern="[A-Za-z0-9_.]+@[A-Za-z0-9_]+\.[A-Za-z0-9]{2,4}" title="usuario@sua_empressa.com" placeholder="usuario@sua_empressa.com" required="" autofocus="">-->
                <input name="j_password" type="password" class="form-control" id="j_password" placeholder="Senha" required="">
                <button class="btn btn-large btn-primary btn-block" type="submit">
                    Entrar
                </button>
            </form>

        </div>
        </div>
    </body>
</html>