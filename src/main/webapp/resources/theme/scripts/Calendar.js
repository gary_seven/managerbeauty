function init() {
	var events = new Array();
	scheduler.config.xml_date = "%Y-%m-%d %H:%i";
	//scheduler.config.readonly = true;
	scheduler.config.first_hour = 8;
	scheduler.config.details_on_dblclick = true;
	scheduler.config.details_on_create = true;

	function callBack(data) {
		events = data.slice();
		var initData = $("#inputData").val();
		var day = initData.substr(0, 2);
		console.log(day);
		var month = initData.substr(3, 3-1);
		console.log(month);
		var year = initData.substr(6, 10);
		console.log(year);
		scheduler.init('scheduler_here', new Date(year,month-1,day), "week");
		scheduler.parse(events, "json");
	}

	$.ajax({
		type : "GET",
		url : "/ManagerBeauty/System/Admin/json",
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		success : function(data) {
			console.log(data);
			callBack(data);
		}
	});
}

var html = function(id) { return document.getElementById(id); }; //just a helper

scheduler.showLightbox = function(id){
	var event = scheduler.getEvent(id);
	scheduler.startLightbox(id, html("my_form"));
	
	html("description").focus();
	html("description").value = event.text;
	//html("custom-prof").value = event.custom1 || "";
	if (!event.custom1)
		$("#custom-prof").val($("#inputProfissional").val());
		console.log($("#inputProfissional").text());
	
	$("#custom-prof").val(event.custom1 || "");
	//html("custom-client").value = event.custom2 || "";
	$("#custom-client").val(event.custom2 || "");
};

function save_form() {
	var event = scheduler.getEvent(scheduler.getState().lightbox_id);
	event.text = html("description").value;
	event.custom1 = html("custom-prof").value;
	event.custom2 = html("custom-client").value;

	scheduler.endLightbox(true, html("my_form"));
}
function close_form() {
	scheduler.endLightbox(false, html("my_form"));
}

function delete_event() {
	var event_id = scheduler.getState().lightbox_id;
	scheduler.endLightbox(false, html("my_form"));
	scheduler.deleteEvent(event_id);
}

