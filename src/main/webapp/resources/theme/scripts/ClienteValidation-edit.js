jQuery.validator.addMethod("verificaCPF", function(value, element) {
	value = value.replace('.', '');
	value = value.replace('.', '');
	cpf = value.replace('-', '');
	while (cpf.length < 11)
		cpf = "0" + cpf;
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (var i = 0; i < 11; i++) {
		a[i] = cpf.charAt(i);
		if (i < 9)
			b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) {
		a[9] = 0;
	} else {
		a[9] = 11 - x;
	}
	b = 0;
	c = 11;
	for (var y = 0; y < 10; y++)
		b += (a[y] * c--);
	if ((x = b % 11) < 2) {
		a[10] = 0;
	} else {
		a[10] = 11 - x;
	}
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])
			|| cpf.match(expReg))
		return false;
	return true;
}, "Informe um CPF válido."); // Mensagem padrão

$(document).ready(function(){
    $("form").validate({
        rules: {
            firstName:{
                minlength: 3,
                maxlength: 20,
                required: true
            },
            lastName:{
                minlength: 3,
                maxlength: 20,
                required: true
            },
            cpf:{
            	required: true,
            	verificaCPF: true
            },
            phone:{
            	required: true
            }
        },
        messages: {
        	firstName: { required: "Informe o primeiro nome", minlength: "No minimo 3 letras" },
        	lastName: { required: "Informe o sobrenome", minlength: "No minimo 3 letras" },
        	cpf: {required: "Informe o cpf"},
        	phone: {required: "Informe o telefone"}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler: function (form){
        	var firstName = $("#firstName").val();
        	var lastName = $("#lastName").val();
        	var cpf = $("#cpf").val();
        	var phone = $("#phone").val();
        	
        	$.ajax({
        		type: "get",
        		url: "/ManagerBeauty/System/Admin/edit-cliente",
        		data: "firstName=" + firstName + "&lastName="
        		+ lastName + "&cpf=" + cpf + "&phone=" + phone,
        		success : function(response) {  
        			console.log(response);
        			$("#message").html(response +
        					"<a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a>");
        			$("#message").addClass("alert alert-success alert-dismissable");
        		},  
        		error : function(e) {  
        			console.log(e);
        			$("#message").html("Algo deu errado no servidor " +
						"<a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a>");
        			$("#message").addClass("alert alert-danger alert-dismissable");
        		}  
        	});
        	return false;
        }
    });
}); 

jQuery(function($){
	$("#phone").mask("(99) 999-9999");
});