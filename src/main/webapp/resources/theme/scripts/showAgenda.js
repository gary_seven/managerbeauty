function getAgenda(){
    
    var request = new createRequest();
    
    if(request === null){
        alert("Incapaz de criar requisição.");
	return;
    }
    var profissional = document.getElementById("inp-funcionarios");
    var data = document.getElementById("datepicker");
    
    var url = "getDates.do?profissional="+escape(profissional.value)+
            "&data="+escape(data.value);
    request.open("POST",url,true);
    request.onreadystatechange = dysplayAgenda;
    request.send(null);
};

function dysplayAgenda(){
    if (request.readyState === 4 && request.status === 200){
        var divAgenda = document.getElementById("agenda");
        divAgenda.innerHTML = request.responseText;
    }
};