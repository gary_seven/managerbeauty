function OpenPopUp(divContainerId, topPosition, opaqueDiv) {
    $obj = $('#' + divContainerId);
    $opaqueBackground = $('#' + opaqueDiv);
    // set CSS properties
    $obj.css({
        'margin': '0 auto',
        'top': topPosition,
        'position': 'fixed',
        'width': '800px',
        'left': '50%',
        'margin-left': '-402px',
        'z-index': '3', 
        'background':'#FFFFFF'
    });

    $opaqueBackground.css({
        'position': 'fixed',
        'top': '0px',
        'width': $opaqueBackground.parent().css('width'),
        'height': $opaqueBackground.parent().parent().css('height'),
        //'background-color':'#5e5e5e',
        'z-index': '2'
    });

    $obj.fadeIn('fast');
    $opaqueBackground.fadeIn('fast');
    
    var servico = $("select#servicos").val();
    var data = $("input#datepicker").val();
    var prof = $("input#inp-funcionarios").val();

    $("input#data").val(data);
    $("input#profissional").val(servico);
    $("input#servico").val(prof);
}
  
