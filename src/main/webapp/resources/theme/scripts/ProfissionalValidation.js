$(document).ready(function(){
    $("form").validate({
        rules: {
            firstName:{
                minlength: 3,
                maxlength: 20,
                required: true
            },
            lastName:{
                minlength: 3,
                maxlength: 20,
                required: true
            },
            email:{
            	required: true,
            	email: true
            },
            phone:{
            	required: true
            },
            logradouro:{
            	required: true,
            	minlength: 3
            },
            cidade:{
            	minlength: 3,
            	maxlength: 20,
            	required: true
            },
            username:{
            	minlength: 3,
            	maxlength: 20,
            	required: true
            },
            senha:{
            	minlength: 5,
            	maxlength: 20,
            	required: true
            }
        },
        messages: {
        	firstName: { required: "Informe o primeiro nome", minlength: "No minimo 3 letras" },
        	lastName: { required: "Informe o sobrenome", minlength: "No minimo 3 letras" },
        	email: {required: "Informe o cpf", email: "Informe um e-mail válido."},
        	logradouro: {required: "Informe o telefone"},
        	cidade: {required: "Informe a cidade"},
        	username: {required: "Informe um nome de usuário", minlength: "No minimo 3 letras"},
        	senha: {required: "Informe uma senha", minlength: "No minimo 5 caracteres."} 
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler: function (form){
        	var firstName = $("#firstName").val();
        	var lastName = $("#lastName").val();
        	var email = $("#email").val();
        	var phone = $("#phone").val();
        	var logradouro = $("#logradouro").val();
        	var cidade = $("#cidade").val();
        	var complemento = $("#complemento").val();
        	var username = $("#username").val();
        	var senha = $("#senha").val();
        	
        	$.ajax({
        		type: "get",
        		url: "/ManagerBeauty/System/Admin/new-profissional",
        		data: "firstName=" + firstName + "&lastName="
        		+ lastName + "&email=" + email + "&phone=" + phone +
        		"&username=" + username + "&senha=" + senha + "&logradouro="
        		+ logradouro + "&complemento=" + complemento + "&cidade="
        		+ cidade,
        		
        		success : function(response) {  
        			console.log(response);
        			$("#message").html(response +
        					"<a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a>");
        			$("#message").addClass("alert alert-success alert-dismissable");
        		},  
        		error : function(e) {  
        			console.log(e);
        			$("#message").html("Algo deu errado no servidor " +
						"<a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a>");
        			$("#message").addClass("alert alert-danger alert-dismissable");
        		}  
        	});
        	return false;
        }
    });
}); 

jQuery(function($){
	$("#phone").mask("(99) 999-9999");
});