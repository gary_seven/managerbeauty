package com.org.managerbeauty.exceptions;

public class ProfissionalnotValidException extends Exception{
	
	static final long serialVersionUID = 1L;

	public ProfissionalnotValidException(String message) {
		super("ProfissionalnotValidException: "+message);
	}
}
