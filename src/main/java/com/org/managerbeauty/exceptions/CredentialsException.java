package com.org.managerbeauty.exceptions;

public class CredentialsException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public CredentialsException(String message) {
		super("VilidationException: "+message);
	}
}
