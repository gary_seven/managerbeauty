package com.org.managerbeauty.exceptions;

public class InvalidCPFException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidCPFException(String message) {
		super("InvalidCPFException: "+message);
	}
}
