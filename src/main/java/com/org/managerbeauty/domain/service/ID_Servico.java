package com.org.managerbeauty.domain.service;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.ValueObject;

public class ID_Servico implements ValueObject<ID_Servico>{

private String id;
	
	public ID_Servico(String id) {
		Validate.notNull(id);
		this.id = id;
	}
	
	public String idString(){
		return id;
	}

	@Override
	public boolean sameValueAs(ID_Servico obj) {
		
		return this.id.equals(obj.id);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if (this.getClass() != obj.getClass())
			return false;
		
		ID_Servico outro = (ID_Servico) obj;
		
		return sameValueAs(outro);
	}
}
