package com.org.managerbeauty.domain.service;

import java.util.List;

public interface IServicoRepository {

	public Servico add (Servico servico);
	
	public Servico find(ID_Servico idServico);
	
	public List<Servico> findByName(String name);
	
	public void remove(Servico servico);
	
	public ID_Servico nextServicoId();
	
	public List<Servico> all();
}
