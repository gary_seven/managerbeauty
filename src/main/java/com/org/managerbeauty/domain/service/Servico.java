package com.org.managerbeauty.domain.service;

import java.util.ArrayList;

import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.shared.Entity;
import com.org.managerbeauty.shared.ValueObject;

public class Servico implements Entity<Servico>{

	ID_Servico id_Servico;
	private String nome;
	private String description = "";
	private float price = 0;
	private static int comisao = 0;
	
	private ArrayList<Profissional> profissionais;
	
	public Servico(ID_Servico id_Servico, String nome, float price) {
		profissionais = new ArrayList<>();
		this.id_Servico = id_Servico;
		this.price = price;
		this.nome = nome;
	}	
	
	public ID_Servico id_Servico(){
		return id_Servico;
	}
	
	public String nome(){
		return nome;
	}
	
	public String description(){
		return description;
	}
	
	public float price (){
		return price;
	}
	
	public static int getComisao() {
		return comisao;
	}
	
	public void setDescription(String descricao){
		this.description = descricao;
	}

	public static void setComisao(int comisao) {
		Servico.comisao = comisao;
	}

	public void cadastrarProfissional(Profissional p){
		profissionais.add(p);
	}
	
	public int getNumberOfProfissionais(){
		return profissionais.size();
	}
	
	public void removerProfissional(Profissional profissional){
		profissionais.remove(profissional);
	}
	
	public ArrayList<Profissional> allProfissionais(){
		return profissionais;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) 
			return false;
		
		if (this.getClass() != obj.getClass())
			return false;
	
		Servico outro = (Servico) obj;
		return samaIdentityAs(outro);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean samaIdentityAs(Servico other) {
		
		return other.id_Servico.equals(other.id_Servico);
	}
}
