package com.org.managerbeauty.domain.entities;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.ValueObject;

public class Usuario implements ValueObject<Usuario>{
	
	private String username;
	private String senha;
	
	public String senha(){
		return senha;
	}
	
	public String username(){
		return username;
	}
	
	public Usuario(String username, String senha) {
		Validate.notNull(username);
		Validate.notNull(senha);
		this.username = username;
		this.senha = senha;
	}

	@Override
	public boolean sameValueAs(Usuario obj) {
		
		return username().equals(obj.username()) 
				&& senha().equals(obj.senha());
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if(!obj.getClass().equals(this.getClass()))
			return false;
		
		Usuario outro = (Usuario)obj;
		return sameValueAs(outro);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
