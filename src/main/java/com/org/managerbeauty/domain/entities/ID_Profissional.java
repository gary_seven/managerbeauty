package com.org.managerbeauty.domain.entities;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.ValueObject;

public class ID_Profissional implements ValueObject<ID_Profissional>{
	
	private String id;
	
	public ID_Profissional(String id) {
		Validate.notNull(id);
		this.id = id;
	}
	
	public String getIdString(){
		return id;
	}

	@Override
	public boolean sameValueAs(ID_Profissional obj) {
		
		return this.id.equals(obj.id);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if (this.getClass() != obj.getClass())
			return false;
		
		ID_Profissional outro = (ID_Profissional) obj;
		
		return sameValueAs(outro);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
