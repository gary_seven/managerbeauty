package com.org.managerbeauty.domain.entities;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.Entity;

public class Cliente implements Entity<Cliente> {

	protected Cliente() {
	}

	private CPF cpf;
	private String firstName;
	private String lastName;
	private String phone;

	public CPF getCpf() {
		return cpf;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setLastName(String lastName) {
		Validate.notNull(lastName);
		this.lastName = lastName;
	}

	public void setPhone(String phone) {
		Validate.notNull(phone);
		this.phone = phone;
	}

	public Cliente(String firstName, final CPF cpf) {
		Validate.notNull(cpf, "CPF é obrigatório");
		Validate.notNull(firstName, "Nome é obrigatório");
		this.firstName = firstName;
		this.cpf = cpf;
	}

	@Override
	public boolean samaIdentityAs(Cliente other) {
		return other.cpf.equals(other.cpf);
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getClass() != obj.getClass())
			return false;

		Cliente other = (Cliente) obj;
		return samaIdentityAs(other);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}