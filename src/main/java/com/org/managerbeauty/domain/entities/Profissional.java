package com.org.managerbeauty.domain.entities;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.Entity;

public class Profissional implements Entity<Profissional>{

	private ID_Profissional id_profissional;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private Endereco endereco;
	private Usuario usuario;
	
	public Profissional(final ID_Profissional id_profissional, String firstName, Usuario usuario) {
		Validate.notBlank(firstName, "Nome é obrigatório");
		Validate.notNull(id_profissional, "id_profissional é obrigatório");
		Validate.notNull(usuario, "usuário obrigatório");
		
		this.id_profissional = id_profissional;
		this.firstName = firstName;
		this.usuario = usuario;
	}
	
	public ID_Profissional getId_profissional (){
		return id_profissional;
	}
	
	public String name(){
		return firstName.concat(" ").concat(lastName);
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public String getPhone(){
		return phone;
	}
	
	public String getEmail(){
		return email;
	}
	
	public Endereco endereco(){
		return endereco;
	}
	
	public void registerLastName(String lastName){
		this.lastName = lastName;
	}
	
	public void registerPhone(String phone){
		Validate.notNull(phone);
		this.phone = phone;
	}
	
	public void registerEmail(String email){
		Validate.notNull(email);
		this.email = email;
	}
	
	public void registerEndereco (Endereco endereco){
		Validate.notNull(endereco);
		this.endereco = endereco;
	}
	
	public Usuario usuario(){
		return usuario;
	}

	@Override
	public boolean samaIdentityAs(Profissional other) {
		
		return getId_profissional().equals(other.getId_profissional());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getClass() != obj.getClass())
			return false;
		
		Profissional other = (Profissional)obj; 
		return samaIdentityAs(other);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
