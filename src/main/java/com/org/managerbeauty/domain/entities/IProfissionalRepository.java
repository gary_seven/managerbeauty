package com.org.managerbeauty.domain.entities;

import java.util.List;

public interface IProfissionalRepository {
	
	public Profissional add (Profissional profissional);
	
	public Profissional find(ID_Profissional matricula);
	
	public List<Profissional> findByName(String name);
	
	public void remove(Profissional profissional);
	
	public Profissional update(Profissional profissional);
	
	public ID_Profissional nextProfissionalId();
	
	public List<Profissional> all();
}
