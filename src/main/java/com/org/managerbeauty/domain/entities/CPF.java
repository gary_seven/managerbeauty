package com.org.managerbeauty.domain.entities;

import com.org.managerbeauty.shared.ValueObject;

public class CPF implements ValueObject<CPF>{
	
	private String cpf;
	
	public CPF(String cpf) {
		this.cpf = cpf;
	}
	
	public String getValue(){
		return cpf;
	}
	
	@Override
	public boolean sameValueAs(CPF obj) {
		
		return cpf.equals(obj.cpf);
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if (this.getClass() != obj.getClass())
			return false;
		
		CPF outro = (CPF)obj;
		return sameValueAs(outro);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
