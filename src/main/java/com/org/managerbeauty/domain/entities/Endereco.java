package com.org.managerbeauty.domain.entities;

import com.org.managerbeauty.shared.ValueObject;

public class Endereco implements ValueObject<Endereco>{
	
	private String logradouro;
	private String complmento;
	private String cidade;
	
	public Endereco(String logradouro, String complemento, String cidade) {		
		this.cidade = cidade;
		this.logradouro = logradouro;
		this.complmento = complemento;
	}
	
	
	
	public String getLogradouro() {
		return logradouro;
	}



	public String getComplmento() {
		return complmento;
	}



	public String getCidade() {
		return cidade;
	}



	@Override
	public boolean sameValueAs(final Endereco outro){
		
		return this.logradouro.equals(outro.logradouro)
				&& this.cidade.equals(outro.cidade)
				&& this.complmento.equals(outro.complmento);
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) 
			return false;
		
		if (this.getClass() != obj.getClass())
			return false;
	
		Endereco outro = (Endereco) obj;
		return sameValueAs(outro);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
