package com.org.managerbeauty.domain.entities;

import java.util.List;

public interface IClienteRepository {

	public Cliente add(Cliente cliente);
	
	public Cliente find(CPF cpf);
	
	public List<Cliente> findByName(String filter);
	
	public void remove(CPF cpf);
	
	public Cliente update(Cliente cliente);
	
	public List<Cliente> all();
}
