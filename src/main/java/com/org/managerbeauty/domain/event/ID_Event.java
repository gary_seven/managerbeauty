package com.org.managerbeauty.domain.event;

import org.apache.commons.lang3.Validate;

import com.org.managerbeauty.shared.ValueObject;

public class ID_Event implements ValueObject<ID_Event>{

	private String id;
	
	public ID_Event(String id) {
		Validate.notNull(id);
		this.id = id;
	}
	
	public String idString(){
		return id;
	}

	@Override
	public boolean sameValueAs(ID_Event obj) {
		
		return this.id.equals(obj.id);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if (this.getClass() != obj.getClass())
			return false;
		
		ID_Event outro = (ID_Event) obj;
		
		return sameValueAs(outro);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
