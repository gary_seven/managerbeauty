package com.org.managerbeauty.domain.event;

import java.util.List;

import org.joda.time.DateTime;

import com.org.managerbeauty.domain.entities.Profissional;

public interface IEventRepository {
	
	public Event add(Event e);
	public Event find(ID_Event idEvent);
	public List<Event> allEventsOf(Profissional p);
	public List<Event> getEventsOfDay(Profissional p, DateTime dTime);
	public ID_Event nextEventId();
}
