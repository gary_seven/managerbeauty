package com.org.managerbeauty.domain.event;

import java.util.ArrayList;

import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;

import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.exceptions.ProfissionalnotValidException;
import com.org.managerbeauty.shared.Entity;

public class Event implements Entity<Event>{
	
	private ID_Event idEvent;
	private Profissional profissional;
	private final Cliente cliente;
	private final Servico servico;
	private DateTime startDate;
	private DateTime endDate;
	
	public Event(ID_Event idEvent, Cliente cliente, Servico servico){
		Validate.noNullElements(new Object[] {idEvent, cliente, servico });
		
		this.idEvent = idEvent;
		this.cliente = cliente;
		this.servico = servico;
		startDate = DateTime.now();
	}
	
	public ID_Event idEvent(){
		return idEvent;
	}

	public DateTime startDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}
	
	public DateTime endDate(){
		return endDate;
	}
	
	public void setEndDate(DateTime endDate){
		this.endDate = endDate;
	}

	public Profissional profissional() {
		return profissional;
	}
	
	public Cliente cliente(){
		return cliente;
	}
	
	public Servico servico(){
		return servico;
	}
	
	public boolean ProfissionalAvalible(Profissional p){
		boolean profissionalDisponivel=true;
		ArrayList<Profissional> profissionais = servico.allProfissionais();
		
		for (Profissional profissional : profissionais) {
			profissionalDisponivel = true;
			if (profissional.equals(p)) break;
			profissionalDisponivel = false;
		}
		return profissionalDisponivel;
	}

	public void assignProfissional(Profissional profissional) throws ProfissionalnotValidException{
		
		if (!ProfissionalAvalible(profissional))
			throw new ProfissionalnotValidException("profissional não disponivel");
		
		this.profissional = profissional;
	}

	@Override
	public boolean samaIdentityAs(Event other) {	
		return idEvent().equals(other.idEvent());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getClass() != obj.getClass())
			return false;
		
		Event other = (Event)obj;
		return samaIdentityAs(other);
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
