package com.org.managerbeauty.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloController {
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView homePage() {
	    return new ModelAndView("index");
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String welcome(ModelMap model){
		
		return "index";
	}
	
}
