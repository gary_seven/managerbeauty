package com.org.managerbeauty.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

@Controller
public class MainController {
	
	@RequestMapping(value="/System/Admin")
	public ModelAndView adminMainPage(){
		
		return new ModelAndView("MB-System/ADM/index");
	}
	
	@RequestMapping(value="/System/Profissional")
	public ModelAndView profissionalMainPage(){
		
		return new ModelAndView("MB-System/Profissional/index");
	}
	
	@RequestMapping(value="/System/Profissional/Agenda")
	public ModelAndView profissionalAgenda(){
		
		return new ModelAndView("MB-System/Profissional/Agenda");
	}

	@RequestMapping(value="/System/Admin/form")
	public ModelAndView adminForm(){
		
		return new ModelAndView("MB-System/ADM/form");
	}
	
	@RequestMapping(value="/System/Admin/Calendario")
	public ModelAndView calendar(){
		
		return new ModelAndView("MB-System/ADM/calendar");
	}
	
	
	@RequestMapping(value="System/Admin/json", method = RequestMethod.GET)
	public ResponseEntity<String> test(final HttpServletResponse response){
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		response.setContentType("application/json; charset=UTF-8");
		
		String content = "[ {id: 1, start_date: \"2014-05-10 12:00\", end_date: \"2014-05-10 14:00\", text:\"Manicure\" },"
				+ "{id: 2, start_date: \"2014-05-05 12:30\", end_date: \"2014-05-05 15:00\", text:\"Corte\" },"
				+ "{id: 3, start_date: \"2014-05-07 13:30\", end_date: \"2014-05-07 17:00\", text:\"Corte\" }, "
				+ "{id: 4, start_date: \"2014-05-15 16:30\", end_date: \"2014-05-15 20:00\", text:\"Pedicure\" }"
				+ "]";
		
		JsonArray jArray = new JsonParser().parse(content).getAsJsonArray();
		
		return new ResponseEntity<String>(jArray.toString(), headers, HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/Graficos")
	public ModelAndView charts(){
		
		return new ModelAndView("MB-System/ADM/charts");
	}
	
	@RequestMapping(value="/System/Admin/Produtos")
	public ModelAndView produtos(){
		
		return new ModelAndView("MB-System/ADM/produtos");
	}
}
