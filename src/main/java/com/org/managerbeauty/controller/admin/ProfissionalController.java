package com.org.managerbeauty.controller.admin;

import java.net.UnknownHostException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.Endereco;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.infrastructure.Fake.FakeProfissionalRepository;
import com.org.managerbeauty.infrastructure.persistance.mongo.ProfissionalRepository;

@Controller
public class ProfissionalController {

	private IProfissionalRepository profissionalRepository;
	
	@PostConstruct
	public void init(){
		
		try {
			profissionalRepository = new ProfissionalRepository();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*private void bootstrap(){
		Profissional profissional = new Profissional(new ID_Profissional("148cf03e"), "Manuela", new Usuario("manuela", "123456"));
		profissional.registerLastName("Fernandes");
		profissional.registerEmail("manuela@gmail.com");
		profissional.registerPhone("(54) 431-6804");
		Endereco endereco = new Endereco("Rua Raimundo Casagrande, 1421", "", "Caxias do Sul-RS");
		profissional.registerEndereco(endereco);
		profissionalRepository.add(profissional);
	}*/
	
	private Profissional biuldProfissional(String firstName, String lastName,String email,
			String phone, Usuario usuario, Endereco endereco){
		
		Profissional profissional = new Profissional(profissionalRepository.nextProfissionalId(), firstName, usuario);
		profissional.registerEmail(email);
		profissional.registerEndereco(endereco);
		profissional.registerPhone(phone);
		return profissional;
	}
	
	private Endereco biuldEndereco(String logradouro, String complemento, String cidade){
		return new Endereco(logradouro, complemento, cidade);
	}
	
	private Usuario biuldUsuario(String username,String senha){
		return new Usuario(username, senha);
	}
	
	@RequestMapping(value="/System/Admin/Profissional")
	public ModelAndView listOfProfissionais(){
		ModelAndView modelAndView = new ModelAndView("MB-System/ADM/listProfissionais");
		
		List<Profissional> profissionais = profissionalRepository.all();
		
		modelAndView.addObject("profissionais", profissionais);
		return modelAndView;
	}
	
	@RequestMapping(value="/System/Admin/Profissional-json")
	public ResponseEntity<String> profissionalJson(final HttpServletResponse response){
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		response.setContentType("application/json; charset=UTF-8");
		
		List<Profissional> profissionais = profissionalRepository.all();
		Gson gson = new Gson();
		String feed = gson.toJson(profissionais);
		return new ResponseEntity<>(feed, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/new-profissional")
	public ResponseEntity<String> newProfissional(@RequestParam(value = "firstName") String firstName,  
			@RequestParam(value = "lastName") String lastName,  
			@RequestParam(value = "email") String email,  
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "username") String username,
			@RequestParam(value = "senha") String senha,
			@RequestParam(value = "logradouro") String logradouro,
			@RequestParam(value = "complemento") String complemento,
			@RequestParam(value = "cidade") String cidade){
		
		Profissional profissional = biuldProfissional(firstName, lastName, email, phone, 
				biuldUsuario(username, senha), biuldEndereco(logradouro, complemento, cidade));
		
		profissionalRepository.add(profissional);
		
		return new ResponseEntity<String>("Profissional criado com sucesso.", HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/profissional-form")
	public ModelAndView profissionalForm(){
		ModelAndView modelAndView = new ModelAndView("MB-System/ADM/newProfissional");
		
		return modelAndView;
	}
}
