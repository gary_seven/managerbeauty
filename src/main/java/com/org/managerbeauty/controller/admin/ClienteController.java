package com.org.managerbeauty.controller.admin;

import java.net.UnknownHostException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.infrastructure.persistance.mongo.ClienteRepository;

@Controller
public class ClienteController {
	
	private ClienteRepository clienteRepository;
	
	@PostConstruct
	public void init(){
		try {
			clienteRepository = new ClienteRepository();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Cliente biuldCliente(String firstName, String lastName, String cpf, String phone){
		Cliente cliente = new Cliente(firstName, new CPF(cpf));
		cliente.setLastName(lastName);
		cliente.setPhone(phone);
		return cliente;
	}

	private String clienteToJson(Cliente cliente){
		Gson gson  = new Gson();
		String json = gson.toJson(cliente);
		return json;
	}
	
	@RequestMapping(value="/System/Admin/cliente-form")
	public ModelAndView addClientePage(){
		return new ModelAndView("MB-System/ADM/Cliente");
	}
	
	@RequestMapping(value="/System/Admin/new-cliente")
	public ResponseEntity<String> addCliente(final HttpServletResponse response,
			@RequestParam(value = "firstName") String firstName,  
			@RequestParam(value = "lastName") String lastName,  
			@RequestParam(value = "cpf") String cpf,  
			@RequestParam(value = "phone") String phone){
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		response.setContentType("application/json; charset=UTF-8");
		
		Cliente cliente = biuldCliente(firstName, lastName, cpf, phone);	
		
		return new ResponseEntity<String>(clienteToJson(clienteRepository.add(cliente)), headers, HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/Cliente")
	public ModelAndView listOfClientes(){
		ModelAndView modelAndView = new ModelAndView("MB-System/ADM/listClientes");
		
		List<Cliente> clientes = clienteRepository.all();
		modelAndView.addObject("clientes", clientes);
		return modelAndView;
	}
	
	@RequestMapping(value="/System/Admin/Cliente-json")
	public ResponseEntity<String> clientesJson(final HttpServletResponse response){
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		response.setContentType("application/json; charset=UTF-8");
		
		List<Cliente> clientes = clienteRepository.all();
		Gson gson = new Gson();
		String feed = gson.toJson(clientes);
		return new ResponseEntity<>(feed, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/edit-cliente/{cpf:.+}", method=RequestMethod.GET)
	public ModelAndView editClientePage(@PathVariable String cpf){
		ModelAndView modelAndView = new ModelAndView("MB-System/ADM/edit-cliente");
		
		CPF cpfObj = new CPF(cpf);
		Cliente cliente = clienteRepository.find(cpfObj);
		modelAndView.addObject("cliente", cliente);
		return modelAndView;
	}
	
	@RequestMapping(value="/System/Admin/edit-cliente", method=RequestMethod.GET)
	public ResponseEntity<String> editClientePage(@RequestParam(value = "firstName") String firstName,  
			@RequestParam(value = "lastName") String lastName,  
			@RequestParam(value = "cpf") String cpf,  
			@RequestParam(value = "phone") String phone){
		
		Cliente cliente = biuldCliente(firstName, lastName, cpf, phone);
		clienteRepository.update(cliente);
		
		return new ResponseEntity<String>("Cliente atualizado", HttpStatus.OK);
	}
	
	@RequestMapping(value="/System/Admin/Cliente/delete/{cpf:.+}", method = RequestMethod.GET)
	public String deleteCliente(@PathVariable String cpf){
		//ModelAndView modelAndView = new ModelAndView("MB-System/ADM/listClientes");
		
		clienteRepository.remove(new CPF(cpf));
		//String message = "Cliente deletado com sucesso.";
		//modelAndView.addObject("message", message);
		return "redirect:/System/Admin/Cliente";
	}
}
