package com.org.managerbeauty.controller.admin;

import java.net.UnknownHostException;
import javax.annotation.PostConstruct;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.org.managerbeauty.application.EventService;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.event.Event;
import com.org.managerbeauty.domain.event.IEventRepository;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.exceptions.ProfissionalnotValidException;
import com.org.managerbeauty.infrastructure.Fake.EventRepositoryFake;
import com.org.managerbeauty.infrastructure.Fake.FakeServicoRepository;
import com.org.managerbeauty.infrastructure.persistance.mongo.ClienteRepository;
import com.org.managerbeauty.infrastructure.persistance.mongo.ProfissionalRepository;

@Controller
public class EventController {

	private IEventRepository eventRepository;
	private EventService eventService;
	
	@PostConstruct
	public void init(){
		eventRepository = new EventRepositoryFake();
		try {
			eventService = new EventService(new EventRepositoryFake(), new ClienteRepository(),
					new ProfissionalRepository(), new FakeServicoRepository());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/System/Admin/new-event")
	public HttpStatus newEvent(@RequestParam(value = "idProfissional") String idProfissional,
			@RequestParam(value = "idServico") String idServico,
			@RequestParam(value = "cpf") String cpf,
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate){
		
		DateTime _startDate = DateTime.parse(startDate, DateTimeFormat.forPattern("yyyy-MM-dd hh:mm"));
		DateTime _endDate = DateTime.parse(endDate, DateTimeFormat.forPattern("yyyy-MM-dd hh:mm"));
		
		Event event = eventService.bookNewEvent(new ID_Servico(idServico), new CPF(cpf), _startDate, _endDate);
		try {
			eventService.assignProfissional(new ID_Profissional(idProfissional), event.idEvent());
		} catch (ProfissionalnotValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return HttpStatus.OK;
	}
}
