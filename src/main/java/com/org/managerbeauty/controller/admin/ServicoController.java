package com.org.managerbeauty.controller.admin;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.infrastructure.Fake.FakeProfissionalRepository;
import com.org.managerbeauty.infrastructure.Fake.FakeServicoRepository;

@Controller
public class ServicoController {

	IServicoRepository servicoRepository;
	//IProfissionalRepository profissionalRepository;
	
	@PostConstruct
	public void init(){
		servicoRepository = new FakeServicoRepository();
		//profissionalRepository = new FakeProfissionalRepository();
		bootstrap();
		
	}
	
	private Profissional profissional(){
		Profissional profissional =  new  Profissional(new ID_Profissional("148cf03e"), "Manuela", new Usuario("manuela", "123456"));
		profissional.registerLastName("");
		return profissional;
	}
	
	private void bootstrap(){
		Servico servico = new Servico(servicoRepository.nextServicoId(),"corte de cabelo", 20);
		servico.setDescription("Corte de cabelo feminino");
		servico.cadastrarProfissional(profissional());
		Servico servico2 = new Servico(servicoRepository.nextServicoId(),"manicure", 10);
		servico2.setDescription("tratamento de beleza das maos");
		Servico servico3 = new Servico(servicoRepository.nextServicoId(),"pedicure", 10);
		servico3.setDescription("tratamento de beleza dos pes");
		
		servicoRepository.add(servico);
		servicoRepository.add(servico2);
		servicoRepository.add(servico3);
	}
	
	@RequestMapping(value="/System/Admin/Servicos")
	public ResponseEntity<String> allServicos(final HttpServletResponse response){
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		response.setContentType("application/json; charset=UTF-8");
		
		Gson gson = new Gson();
		String feed = gson.toJson(servicoRepository.all());
		
		return new ResponseEntity<>(feed, headers, HttpStatus.OK );
	}
}
