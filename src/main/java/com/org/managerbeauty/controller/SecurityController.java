package com.org.managerbeauty.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SecurityController {
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(){
		
		return new ModelAndView("Login");
	}

	@RequestMapping(value="/success-login")
    public String successLogin(HttpServletRequest request) {
		
		if (request.isUserInRole("ROLE_ADMIN")) return "redirect:/System/Admin";
		
		else if (request.isUserInRole("ROLE_USER")) return "redirect:/System/Profissional/Agenda";
		
        return "";
    }
	
	@RequestMapping(value="/error-login")
	public ModelAndView invalidLogin(){
		
		ModelAndView modelAndView = new ModelAndView("Login");
		modelAndView.addObject("error",true);
		return modelAndView;
	}
}
