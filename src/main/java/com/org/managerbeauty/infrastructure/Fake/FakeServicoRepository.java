package com.org.managerbeauty.infrastructure.Fake;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;

public class FakeServicoRepository implements IServicoRepository{

	private ArrayList<Servico> servicos;
	
	public FakeServicoRepository() {
		servicos = new ArrayList<>();
	}
	
	@Override
	public Servico add(Servico servico) {
		servicos.add(servico);
		return null;
	}

	@Override
	public Servico find(ID_Servico idServico) {
		LinkedList<Servico> searchResult = new LinkedList<>();
		
		for (Servico servico : servicos) {
			if(servico.id_Servico().equals(idServico)){
				searchResult.add(servico);
			}
			break;
		}
		return searchResult.getFirst();
	}

	@Override
	public List<Servico> findByName(String name) {
		LinkedList<Servico> searchResult = new LinkedList<>();
		
		for (Servico servico : servicos) {
			if(servico.nome().contains(name))
				searchResult.add(servico);
		}
		return searchResult;
	}

	@Override
	public void remove(Servico servico) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ID_Servico nextServicoId() {
		final String random = UUID.randomUUID().toString().toLowerCase();
		
		return new ID_Servico(random.substring(0, random.indexOf("-")));
	}

	@Override
	public List<Servico> all() {
		
		return servicos;
	}

}
