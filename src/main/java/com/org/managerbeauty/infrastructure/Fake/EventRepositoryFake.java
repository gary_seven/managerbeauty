package com.org.managerbeauty.infrastructure.Fake;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.event.Event;
import com.org.managerbeauty.domain.event.ID_Event;
import com.org.managerbeauty.domain.event.IEventRepository;

public class EventRepositoryFake  implements IEventRepository{
	
	private ArrayList<Event> _theEvents;
	
	public EventRepositoryFake() {
		_theEvents = new ArrayList<>();
	}
	
	public Event add(Event event){
		_theEvents.add(event);
		return event;
	}
	
	public List<Event> allEventsOf(Profissional profissional){
		ArrayList<Event> eventsOf = new ArrayList<>();
		
		for (Event e : _theEvents) {
			if (e.profissional().samaIdentityAs(profissional))
				eventsOf.add(e);
		}
		return eventsOf;
	}
	
	public List<Event> getEventsOfDay(Profissional p, DateTime dTime){
		ArrayList<Event> eventsList = new ArrayList<>();
		
		for (Event event : eventsList) {
			if (event.profissional().samaIdentityAs(p) && event.startDate().equals(dTime)) {
				eventsList.add(event);
			}
		}
		return eventsList;
	}

	@Override
	public ID_Event nextEventId() {
		final String random = UUID.randomUUID().toString().toLowerCase();
		
		return new ID_Event(random.substring(0, random.indexOf("-")));
	}

	@Override
	public Event find(ID_Event idEvent) {
		// TODO Auto-generated method stub
		return null;
	}
}
