package com.org.managerbeauty.infrastructure.Fake;

import java.util.List;
import java.util.UUID;

import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;

public class FakeProfissionalRepository implements IProfissionalRepository{

	
	@Override
	public Profissional add(Profissional profissional) {
		return null;
	}

	@Override
	public Profissional find(ID_Profissional matricula) {
		
		return null;
	}

	@Override
	public List<Profissional> findByName(String name) {
		
		return null;
	}

	@Override
	public void remove(Profissional profissional) {
		
	}

	@Override
	public ID_Profissional nextProfissionalId() {
		final String random = UUID.randomUUID().toString().toLowerCase();
		
		return new ID_Profissional(random.substring(0, random.indexOf("-")));
	}

	@Override
	public Profissional update(Profissional profissional) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Profissional> all() {
		// TODO Auto-generated method stub
		return null;
	}

}
