package com.org.managerbeauty.infrastructure.Fake;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.IClienteRepository;

public class FakeClienteRepository implements IClienteRepository{

	private ArrayList<Cliente> clientes;
	
	public FakeClienteRepository() {
		clientes = new ArrayList<>();
	}
	
	@Override
	public Cliente add(Cliente cliente) {
		clientes.add(cliente);
		return cliente;
	}

	@Override
	public Cliente find(CPF cpf) {
		return null;
	}
	
	@Override
	public List<Cliente> findByName(String filter) throws NullPointerException {
		String name = "";
		LinkedList<Cliente> searchResult = new LinkedList<>();
		
		for (Cliente cliente : clientes) {
			name = cliente.getFirstName().concat(" "+cliente.getLastName());
			
			if(name.contains(filter))
				searchResult.add(cliente);
		}
		return searchResult;
	}

	@Override
	public void remove(CPF cpf) {
		
	}

	@Override
	public List<Cliente> all() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente update(Cliente cliente) {
		// TODO Auto-generated method stub
		return null;
	}

}
