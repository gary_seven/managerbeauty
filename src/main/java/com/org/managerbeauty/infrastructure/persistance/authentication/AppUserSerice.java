package com.org.managerbeauty.infrastructure.persistance.authentication;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.org.managerbeauty.exceptions.CredentialsException;
import com.org.managerbeauty.infrastructure.persistance.mysql.MySQL;

public class AppUserSerice {

	private static MySQL database;
	
	public AppUserSerice() {
		database = new MySQL("localhost", "security", "root", "root");
	}
	
	private static ResultSet checkIfExist(String username) throws SQLException{
		String query = String.format("SELECT USER_ID, USERNAME FROM users WHERE USERNAME = %s;", username);
		ResultSet rs;
		try {
			database.connect();
			rs = database.execute(query);
		} catch (SQLException e) {
			throw new SQLException();
		}
		return rs;
	}
	
	public static AppUser find(String username) throws SQLException, CredentialsException{
		String query = String.format("SELECT USERNAME, PASSWORD, SALAO_ID, AUTHORITY"
								+ "	FROM users u JOIN user_roles ur"
								+ " ON u.USER_ID = ur.USER_ID AND u.USERNAME = %s;", username);
		
		try {
			database.connect();
			ResultSet rs = database.execute(query);
			
			if (rs != null) {
				AppUser appUser = new AppUser();
				appUser.setAutority(rs.getString("AUTHORITY"));
				appUser.setPassword(rs.getString("PASSWORD"));
				appUser.setSalaoId(rs.getString("SALAO_ID"));
				appUser.setUserName(rs.getString("USERNAME"));
				
				return appUser;
			}else throw new CredentialsException("Error while validating");
			
		} catch (SQLException e) {
			throw new SQLException();
		}
	}
	
	public static void insert(String username, String password, String salaoId, String authority) throws SQLException{
		ResultSet rs = checkIfExist(username);
		if (!rs.next()){
			String query = String.format("INSERT INTO users (USERNAME, PASSWORD, SALAO_ID) "
										+ "VALUES (%s, %s, %s, %s);", username, password, salaoId);
			try {
				database.connect();
				database.inserir(query);
				
				rs = checkIfExist(username);
				int userId = rs.getInt("USER_ID");
				query = String.format("INSERT INTO user_roles (USER_ID, AUTHORITY) "
									+ "VALUES (%i, %s);", userId, authority);
				database.inserir(query);
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
	}
}
