package com.org.managerbeauty.infrastructure.persistance.authentication;

import org.apache.commons.lang3.Validate;

public class AppUser {

	private String userName;
	private String password;
	private String salaoId;
	private String autority;
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		Validate.notBlank("This username is blank!",userName);
		this.userName = userName;
	}
	
	public String getPassword() {
		Validate.notBlank("This password blank!",password);
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSalaoId() {
		return salaoId;
	}
	
	public void setSalaoId(String salaoId) {
		Validate.notBlank("This salaoId blank!",salaoId);
		this.salaoId = salaoId;
	}
	
	public String getAutority() {
		return autority;
	}
	
	public void setAutority(String autority) {
		Validate.notBlank("This permission is not valid!",autority);
		this.autority = autority;
	}
}
