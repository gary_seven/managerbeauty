package com.org.managerbeauty.infrastructure.persistance.mongo;

import java.net.UnknownHostException;
import java.util.List;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;

public class ServicoRepository implements IServicoRepository {
	
	private final DB base;
	private final DBCollection servicos;

	public ServicoRepository() throws UnknownHostException {
		base = MongoDB.getBase();
		servicos = base.getCollection("servicos");
	}
	
	@Override
	public Servico add(Servico servico) {
		BasicDBObject newServico = newServicoDocument(servico);
		servicos.insert(newServico);
		return servico;
	}

	@Override
	public Servico find(ID_Servico idServico) {
		
		return null;
	}

	@Override
	public List<Servico> findByName(String name) {
		
		return null;
	}

	@Override
	public void remove(Servico servico) {
	}

	@Override
	public ID_Servico nextServicoId() {
		
		return null;
	}

	private BasicDBObject newServicoDocument(Servico servico){
		BasicDBObject newServicoObject = new BasicDBObject();
		newServicoObject.put("idServico", servico.id_Servico().idString());
		newServicoObject.put("name", servico.nome());
		newServicoObject.put("description", servico.description());
		newServicoObject.put("price", servico.price());
		newServicoObject.put("comition", servico.getComisao());
		return newServicoObject;
	}
	
	private Servico toServicoObject(DBObject servico){
		Servico servicoObj = new Servico(new ID_Servico(servico.get("idServico").toString()), 
				servico.get("name").toString(), (Float)servico.get("price"));
		
		servicoObj.setDescription(servico.get("description").toString());
		servicoObj.setComisao((int)servico.get("comition"));
		return servicoObj;
	}

	@Override
	public List<Servico> all() {
		// TODO Auto-generated method stub
		return null;
	}
}
