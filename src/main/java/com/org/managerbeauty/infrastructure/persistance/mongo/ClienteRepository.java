package com.org.managerbeauty.infrastructure.persistance.mongo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.IClienteRepository;

public class ClienteRepository implements IClienteRepository {

	private final DB base;
	private final DBCollection clientes;
	
	public ClienteRepository() throws UnknownHostException {
		base = MongoDB.getBase();
		clientes = base.getCollection("clientes");
	}
	
	@Override
	public Cliente add(Cliente cliente) {
		BasicDBObject newCliente = newClieteDocument(cliente);
		clientes.insert(newCliente);
		return cliente;
	}

	@Override
	public Cliente find(CPF cpf) {
		BasicDBObject query = new BasicDBObject("cpf", cpf.getValue());
		return toClientObject(clientes.findOne(query));
	}

	@Override
	public List<Cliente> findByName(String filter) {
		List<Cliente> selectedClientes = all();
		String nome;
		for (Cliente cliente : all()) {
			nome  = cliente.getFirstName().concat(" ").concat(cliente.getLastName());
			if(nome.contains(filter)){
				selectedClientes.add(cliente);
			}
		}
		return selectedClientes;
	}

	@Override
	public void remove(CPF cpf) {
		BasicDBObject query = new BasicDBObject("cpf",cpf.getValue());
		clientes.remove(query);
	}

	@Override
	public List<Cliente> all() {
		List<Cliente> selectedClientes = new ArrayList<>();
		DBCursor cursor = clientes.find();
		while (cursor.hasNext()) {
			selectedClientes.add(toClientObject(cursor.next()));
		}
		return selectedClientes;
	}
	
	private Cliente toClientObject(DBObject cliente){
		Cliente clienteObj = new Cliente(cliente.get("firstName").toString(), 
				new CPF(cliente.get("cpf").toString()));
		
		clienteObj.setLastName(cliente.get("lastName").toString());
		clienteObj.setPhone(cliente.get("phone").toString());
		
		return clienteObj;
	}

	@Override
	public Cliente update(Cliente cliente) {
		BasicDBObject query = new BasicDBObject("cpf",cliente.getCpf().getValue());
		BasicDBObject newCliente = newClieteDocument(cliente);
		BasicDBObject updatedCliente = new BasicDBObject("$set", newCliente);
		
		clientes.update(query, updatedCliente);
		return cliente;
	}
	
	private BasicDBObject newClieteDocument(Cliente cliente){
		BasicDBObject newCliente = new BasicDBObject();
		newCliente.put("cpf", cliente.getCpf().getValue());
		newCliente.put("firstName", cliente.getFirstName());
		newCliente.put("lastName", cliente.getLastName());
		newCliente.put("phone", cliente.getPhone());
		return newCliente;
	}
}
