package com.org.managerbeauty.infrastructure.persistance.mongo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.org.managerbeauty.domain.entities.Endereco;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;

public class ProfissionalRepository implements IProfissionalRepository {
	
	private final DB base;
	private final DBCollection profissionais;
	
	public ProfissionalRepository() throws UnknownHostException {
		base = MongoDB.getBase();
		profissionais = base.getCollection("profissionais"); 
	}
	
	@Override
	public Profissional add(Profissional profissional) {
		BasicDBObject profissionalObj = newProfissionalDocument(profissional);
		profissionais.insert(profissionalObj);
		return profissional;
	}

	@Override
	public Profissional find(ID_Profissional matricula) {
		
		return null;
	}

	@Override
	public List<Profissional> findByName(String name) {
		
		return null;
	}

	@Override
	public void remove(Profissional profissional) {
		
		
	}

	@Override
	public Profissional update(Profissional profissional) {
		
		return null;
	}

	@Override
	public ID_Profissional nextProfissionalId() {
		final String random = UUID.randomUUID().toString().toLowerCase();
		
		return new ID_Profissional(random.substring(0, random.indexOf("-")));
	}
	
	private Profissional toProfissionalObject(DBObject obj){
		Profissional profissional = new Profissional(new ID_Profissional(obj.get("idProfissional").toString()), 
				obj.get("firstName").toString(), new Usuario(obj.get("usuario").toString(), obj.get("senha").toString()));
		
		profissional.registerEmail(obj.get("email").toString());
		profissional.registerPhone(obj.get("phone").toString());
		profissional.registerEndereco(
				new Endereco(obj.get("logradouro").toString(), obj.get("complemento").toString(), obj.get("cidade").toString()));
		return profissional;
	}
	
	private BasicDBObject newProfissionalDocument(Profissional profissional){
		BasicDBObject newProfissional = new BasicDBObject();
		newProfissional.put("idProfissional", profissional.getId_profissional().getIdString());
		newProfissional.put("firstName", profissional.getFirstName());
		newProfissional.put("phone", profissional.getPhone());
		newProfissional.put("email", profissional.getEmail());
		newProfissional.put("cidade", profissional.endereco().getCidade());
		newProfissional.put("logradouro", profissional.endereco().getLogradouro());
		newProfissional.put("complemento", profissional.endereco().getComplmento());
		newProfissional.put("usuario", profissional.usuario().username());
		newProfissional.put("senha", profissional.usuario().senha());
		
		return newProfissional;
	}

	@Override
	public List<Profissional> all() {
		List<Profissional> selectedProfissionais = new ArrayList<>();
		DBCursor dbCursor = profissionais.find();
		while (dbCursor.hasNext()) {
			selectedProfissionais.add(toProfissionalObject(dbCursor.next()));
		}
		return selectedProfissionais;
	}

}
