package com.org.managerbeauty.infrastructure.persistance.mysql;

import java.nio.channels.IllegalSelectorException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;

public class MySQL {
	
	private String host;
    private String user;
    private String pass;
    private String database;
    
    private Connection conn;
    
    /**
     * Construtor da classe
     * 
     * @param host Host em que se deseja conectar 
     * @param database Nome do database em que se deseja conectar
     * @param user Nome do usuário
     * @param pass Senha do usuário
     */
    public MySQL( String host, String database, String user, String pass ) {
        this.pass = pass;
        this.user = user;
        this.host = host;
        this.database = database;
    }
    
    /**
     * Método que estabelece a conexão com o banco de dados
     * 
     * @return True se conseguir conectar, falso em caso contrário.
     * @throws SQLException 
     */
    public void connect() throws SQLException {
        String url;
        
        url = "jdbc:mysql://"+this.host+"/"
              +this.database+"?"
              +"user="+this.user
              +"&password="+this.pass;
              
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            this.conn = (Connection) DriverManager.getConnection(url);
            
        } catch( SQLException e ) {               
            throw new SQLException("Error while geting Mysql connection!");
            
        } catch ( ClassNotFoundException e ) {
            throw new ClassCastException();
            
        } catch ( InstantiationException e ) {        
            throw new IllegalSelectorException();
            
        } catch ( IllegalAccessException e ) {
            throw new IllegalSelectorException();
        }
    }
    
    /**
     *  Método que executa uma query
     * 
     *  @param query String que represente um comando SQL
     *  @return objeto ResultSet que contém o resultado da query
     */
    public ResultSet execute(String query) throws SQLException{
    	Statement st;
    	ResultSet rs;
    	
    	try {
			st = this.conn.createStatement();
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			throw new SQLException();
		}
    	return rs;
    }
    
    
    /**
     * Executa uma query como update, delete ou insert.
     * Retorna o número de registros afetados quando falamos de um update ou delete
     * ou retorna 1 quando o insert é bem sucedido. Em outros casos retorna -1
     * 
     * @param query A query que se deseja executar
     * @return 0 para um insert bem sucedido. -1 para erro
     */
    public void inserir(String query) throws SQLException{
    	
    	try {
    		Statement st = this.conn.createStatement();
		} catch (SQLException e) {
			throw new SQLException();
		}
    }
}