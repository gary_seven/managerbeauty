package com.org.managerbeauty.infrastructure.persistance.mongo;

import java.net.UnknownHostException;
import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoDB {

	private static final String database = "managerBeauty";
	private static final String host = "localhost";
	private static final int port = 27017;
	
	private MongoDB(){}
	
	public static DB getBase() throws UnknownHostException{
		MongoClient mongo = new MongoClient(host,port);
		DB base = mongo.getDB(database);
		
		return base;
	}	
}
