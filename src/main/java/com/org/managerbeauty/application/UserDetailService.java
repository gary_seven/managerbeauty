package com.org.managerbeauty.application;

import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import com.org.managerbeauty.exceptions.CredentialsException;
import com.org.managerbeauty.exceptions.UserNotFoundException;
import com.org.managerbeauty.infrastructure.persistance.authentication.AppUser;
import com.org.managerbeauty.infrastructure.persistance.authentication.AppUserSerice;

public class UserDetailService implements UserDetailsService{
	
	public UserDetailService() {
		
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException, DataAccessException {
		AppUser appUser = null;
		try {
			appUser = AppUserSerice.find(login);
			
			if (appUser == null) throw new UserNotFoundException("account name not found");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CredentialsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return biuldUser(appUser);
	}
	
	//@SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
	private User biuldUser(AppUser appUser){
		
    	String username = appUser.getUserName();
    	String password = appUser.getPassword();
    	
    	boolean enabled = true;  
        boolean accountNonExpired = true;  
        boolean credentialsNonExpired = true;  
        boolean accountNonLocked = true;
        
        //aditional information
        String salaoId = appUser.getSalaoId(); 
        
		return null;
	}
}
