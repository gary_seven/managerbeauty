package com.org.managerbeauty.application;

import org.joda.time.DateTime;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.IClienteRepository;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.event.Event;
import com.org.managerbeauty.domain.event.ID_Event;
import com.org.managerbeauty.domain.event.IEventRepository;
import com.org.managerbeauty.domain.service.ID_Servico;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.exceptions.ProfissionalnotValidException;

public final class EventService {

	private IEventRepository eventRepository;
	private IClienteRepository clienteRepository;
	private IProfissionalRepository profissionalRepository;
	private IServicoRepository servicoRepository;
	
	public EventService(final IEventRepository eventRepository,
						final IClienteRepository clienteRepository,
						final IProfissionalRepository profissionalRepository,
						final IServicoRepository servicoRepository) {
		
		this.eventRepository = eventRepository;
		this.clienteRepository = clienteRepository;
		this.profissionalRepository = profissionalRepository;
		this.servicoRepository = servicoRepository;
	}
	
	public Event bookNewEvent(ID_Servico idServico, 
								CPF cpf, DateTime startDate, DateTime endDate){
		
		ID_Event idEvent = eventRepository.nextEventId();
		Cliente cliente = clienteRepository.find(cpf);
		Servico servico = servicoRepository.find(idServico);
		
		Event event = new Event(idEvent, cliente, servico);
		event.setStartDate(startDate);
		event.setEndDate(endDate);
		
		return eventRepository.add(event);
	}
	
	public void assignProfissional(ID_Profissional idProfissional, ID_Event idEvent) throws ProfissionalnotValidException{
		Profissional profissional = profissionalRepository.find(idProfissional);
		Event event = eventRepository.find(idEvent);
		
		try {
			event.assignProfissional(profissional);
		} catch (ProfissionalnotValidException e) {
			
			throw new ProfissionalnotValidException("");
		}
	}
}
