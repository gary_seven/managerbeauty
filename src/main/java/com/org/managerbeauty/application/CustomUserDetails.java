package com.org.managerbeauty.application;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUserDetails extends User{

	private static final long serialVersionUID = 1L;
	private String salaoId;
	
	public CustomUserDetails(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, String salaoId) {
		
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		
		this.salaoId = salaoId;
	}

	public void setSalaoId(String salaoId){
		this.salaoId = salaoId;
	}
	
	public String getSalaoId(){
		return this.salaoId;
	}
}
