package com.org.managerbeauty.shared;

public interface Entity<T> {

	public boolean samaIdentityAs(T other);
}
