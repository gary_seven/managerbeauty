package com.org.managerbeauty.shared;

public interface ValueObject<T> {
	
	boolean sameValueAs(final T obj);
}
