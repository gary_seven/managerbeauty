package bootstrap;

import java.net.UnknownHostException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.org.managerbeauty.domain.entities.CPF;
import com.org.managerbeauty.domain.entities.Cliente;
import com.org.managerbeauty.domain.entities.Endereco;
import com.org.managerbeauty.domain.entities.ID_Profissional;
import com.org.managerbeauty.domain.entities.IProfissionalRepository;
import com.org.managerbeauty.domain.entities.Profissional;
import com.org.managerbeauty.domain.entities.Usuario;
import com.org.managerbeauty.domain.event.Event;
import com.org.managerbeauty.domain.event.ID_Event;
import com.org.managerbeauty.domain.service.IServicoRepository;
import com.org.managerbeauty.domain.service.Servico;
import com.org.managerbeauty.infrastructure.Fake.FakeServicoRepository;
import com.org.managerbeauty.infrastructure.persistance.mongo.ClienteRepository;
import com.org.managerbeauty.infrastructure.persistance.mongo.ProfissionalRepository;

public class Bootstrap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IProfissionalRepository profissionalRepository = null;
		try {
			profissionalRepository = new ProfissionalRepository();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Profissional> profissionais = profissionalRepository.all();
		Gson gson = new Gson();
		String feed = gson.toJson(profissionais);
		
		System.out.println(feed);
	}

}
